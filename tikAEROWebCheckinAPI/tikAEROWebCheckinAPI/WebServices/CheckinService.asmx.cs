﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Data;
using System.Xml.XPath;
using System.Xml.Xsl;
using System.IO;
using System.Text;
using System.Configuration;
using tikSystem.Web.Library;
using tikSystem.Web.Library.agentservice;
using tikAEROWebCheckinAPI.Classes;
using Core.Lib.Crypto;
using System.Xml;

namespace tikAEROWebCheckinAPI.WebServices
{
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ScriptService]  
    public class CheckinService : System.Web.Services.WebService
    {

        [WebMethod(EnableSession = true, Description = "Step 1 : Logon system")]
        public APIResult Logon(string strBookingRef, string strLastName)
        {
            Library objLi = new Library();
            Booking objBooking = new Booking();
            string strXml = string.Empty;
            string bookingSegments = string.Empty;

            bool isParameterWellForm = true;
            APIFlightSegments fss = new APIFlightSegments();
            APIPassengerFees fees = new APIPassengerFees();
            APIErrors errors = new APIErrors();
            string parametersForLog = strBookingRef + "|" + strLastName;

            if (string.IsNullOrEmpty(strBookingRef))
            {
                isParameterWellForm = false;
                GetAPIErrors("110", errors);
                LogHelper.writeToLogFile("Logon", parametersForLog, "110", "BookingRef is empty", string.Empty);
            }
            else if (strBookingRef.Length == 10)
            {
                strBookingRef = strBookingRef.Substring(0, 9);
            }

            if (isParameterWellForm)
            {
                try
                {
                    InitializeService();
                    objBooking.objService = (TikAeroXMLwebservice)Session["AgentService"];
                    DataSet ds = objBooking.BookingLogon(strBookingRef.Trim(), strLastName.Trim());

                    if (ds != null)
                    {
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            Flights objFlights = new Flights();
                            objFlights.objService = (TikAeroXMLwebservice)Session["AgentService"];
                            string xmlResult = objFlights.GetBookingSegmentCheckIn(ds.Tables[0].Rows[0]["booking_id"].ToString(),
                                                                                   string.Empty,
                                                                                   string.Empty);

                            Session["CheckInFlight"] = xmlResult;
                            XPathDocument xmlDoc = new XPathDocument(new StringReader(xmlResult));
                            if (GetOutstandingBalance(xmlDoc) <= 0)
                            {
                                XPathNavigator nv = xmlDoc.CreateNavigator();
                                if (nv.Select("Booking/Mapping[segment_status_rcd = 'HK'][flight_check_in_status_rcd = 'OPEN']").Count == 0)
                                {
                                    GetAPIErrors("103", errors); //Flight not open for checkin
                                    LogHelper.writeToLogFile("Logon", parametersForLog, "103", "Flight not open for checkin", string.Empty);
                                }
                                else
                                {
                                    XPathExpression xe = nv.Compile("Booking/Mapping");
                                    xe.AddSort("booking_segment_id", XmlSortOrder.Ascending, XmlCaseOrder.None, string.Empty, XmlDataType.Text);

                                    foreach (XPathNavigator n in nv.Select(xe))
                                    {
                                        string strBookingSegmentId = objLi.getXPathNodevalue(n, "booking_segment_id", Library.xmlReturnType.value);
                                        if (bookingSegments.IndexOf(strBookingSegmentId) < 0)
                                        {
                                            //GetFlightInformation(strBookingSegmentId);
                                            bookingSegments += strBookingSegmentId + "|";
                                        }
                                    }

                                    string strGetPassengerDetailXML = string.Empty;
                                    string[] strBookingSegmentIds = bookingSegments.Split('|');
                                    if (strBookingSegmentIds.Length > 0)
                                    {
                                        foreach (string bookSegment in strBookingSegmentIds)
                                        {
                                            if (bookingSegments != "")
                                            {
                                                strGetPassengerDetailXML = GetFlightInformation(bookSegment);
                                                GetAPIFlightSegments(bookSegment, strGetPassengerDetailXML, xmlResult, fss);
                                                GetAPIPassengerFees(bookSegment, strGetPassengerDetailXML, fees);
                                            }
                                        }
                                        GetAPIErrors("000", errors);
                                    }
                                }
                            }
                            else
                            {
                                GetAPIErrors("102", errors); //Have outstanding balance cannnot login
                                LogHelper.writeToLogFile("Logon", parametersForLog, "102", "Have outstanding balance cannnot login", string.Empty);
                            }
                        }
                        else
                        {
                            GetAPIErrors("101", errors);
                            LogHelper.writeToLogFile("Logon", parametersForLog, "101", "BookingLogon fail (dataset has no table/row)", string.Empty);
                        }
                    }
                    else
                    {
                        GetAPIErrors("101", errors);
                        LogHelper.writeToLogFile("Logon", parametersForLog, "101", "BookingLogon fail (dataset is null)", string.Empty);
                    }
                }
                catch (Exception ex)
                {
                    GetAPIErrors("100", errors);
                    LogHelper.writeToLogFile("Logon", parametersForLog, "100", ex.Message, ex.StackTrace);
                }
            }

            return objLi.BuildAPIResultXML(fss, null, null, null, fees, null, null, errors);
        }

        [WebMethod(EnableSession = true, Description = "Step 2 : Read passenger information")]
        public APIResult GetPassengers(string strBookingSegmentId)
        {
            // remove session of seat both auto assign and select seat
            if (Session["SaveMapping"] != null)
                Session.Remove("SaveMapping");

            // fix case sensitive
            if (!string.IsNullOrEmpty(strBookingSegmentId))
                strBookingSegmentId = strBookingSegmentId.ToLower();

            Library objLi = new Library();
            string strGetPassengerDetailXML = string.Empty;

            APIPassengerMappings mappings = new APIPassengerMappings();
            APIPassengerServices services = new APIPassengerServices();
            APIErrors errors = new APIErrors();
            bool isParameterWellForm = true;
            string codeMessage = "201"; // not found passenger information
            string parametersForLog = strBookingSegmentId;

            if (string.IsNullOrEmpty(strBookingSegmentId))
            {
                isParameterWellForm = false;
                GetAPIErrors("210", errors);
                LogHelper.writeToLogFile("GetPassengers", parametersForLog, "210", "BookingSegmentId is empty", string.Empty);
            }

            if (isParameterWellForm)
            {
                try
                {
                    //InitializeService();
                    strGetPassengerDetailXML = GetFlightInformation(strBookingSegmentId);
                    if (strGetPassengerDetailXML != "<Booking />")
                    {
                        GetAPIPassengerMappings(strBookingSegmentId, "", strGetPassengerDetailXML, mappings);
                        GetAPIPassengerServices(strBookingSegmentId, "", strGetPassengerDetailXML, services);
                        codeMessage = "000";
                    }
                    GetAPIErrors(codeMessage, errors);

                    if (codeMessage != "000")
                    {
                        LogHelper.writeToLogFile("GetPassengers", parametersForLog, "201", "Not found passenger information", string.Empty);
                    }
                }
                catch(Exception ex)
                {
                    GetAPIErrors("200", errors);
                    LogHelper.writeToLogFile("GetPassengers", parametersForLog, "200", ex.Message, ex.StackTrace);
                }
            }
            return objLi.BuildAPIResultXML(null, mappings, null, services, null, null, null, errors);
        }

        [WebMethod(EnableSession = true, Description = "Initial Token")]
        public APIResultMessage InitialToken(string strAgencyCode, string strAgencyLogon, string strAgencyPassword)
        {
            Library objLi = new Library();
            Users users = new Users();
            Agents agents = new Agents();
            String strResult = String.Empty;
            String strGetPassengerDetailXML = String.Empty;

            DataSet ds = new DataSet();
            APIErrors errors = new APIErrors();
            bool isParameterWellForm = true;
            String parametersForLog = String.Empty;
            String userId = String.Empty;
            String codeMessage = "950";
            String hky = String.Empty;

            if (string.IsNullOrEmpty(strAgencyCode))
            {
                isParameterWellForm = false;
                GetAPIErrors("951", errors);
                LogHelper.writeToLogFile("InitialToken", parametersForLog, "951", "Invalid AgencyCode parameter", string.Empty);
            }
            else if (string.IsNullOrEmpty(strAgencyLogon))
            {
                isParameterWellForm = false;
                GetAPIErrors("952", errors);
                LogHelper.writeToLogFile("InitialToken", parametersForLog, "952", "Invalid AgencyLogon parameter", string.Empty);
            }
            else if (string.IsNullOrEmpty(strAgencyPassword))
            {
                isParameterWellForm = false;
                GetAPIErrors("953", errors);
                LogHelper.writeToLogFile("InitialToken", parametersForLog, "953", "Invalid AgencyPassword parameter", string.Empty);
            }

            if (isParameterWellForm)
            {
                try
                {
                    String hk = String.Empty;
                    ServiceClient objService = new ServiceClient();
                    objService.initializeWebService(strAgencyCode, ref agents);

                    if (agents != null && agents.Count > 0)
                    {
                        if (agents[0].api_flag == 1) 
                        {
                            users = objService.TravelAgentLogon(strAgencyCode, strAgencyLogon, strAgencyPassword);

                            if (users != null && users.Count > 0)
                            {
                                if (System.Configuration.ConfigurationManager.AppSettings["hashkey"] != null)
                                {
                                    hk = System.Configuration.ConfigurationManager.AppSettings["hashkey"].ToString();
                                }
                                userId = users[0].user_account_id.ToString();
                                String temp = StringCipher.Encrypt(userId, hk);
                                codeMessage = "000";
                                GetAPIErrors(codeMessage, temp, errors);
                                LogHelper.writeToLogFile("InitialToken", parametersForLog, "000", "Token = " + temp, "Token = " + temp);
                            }
                            else
                            {
                                codeMessage = "950";
                            }
                        }else
                        {
                            codeMessage = "954";
                        }
                    }

                    
                    
                    if (codeMessage != "000")
                    {
                        LogHelper.writeToLogFile("InitialToken", parametersForLog, "950", "Invalid initial token", string.Empty);
                        GetAPIErrors(codeMessage, errors);
                    }
                    
                }
                catch (Exception ex)
                {
                    GetAPIErrors("950", errors);
                    LogHelper.writeToLogFile("InitialToken", parametersForLog, "950", ex.Message, ex.StackTrace);
                }
            }

            return objLi.BuildAPIResultXML(errors);
        }

        [WebMethod(EnableSession = true, Description = "Board Passenger")]
        public APIResultMessage BoardPassengers(string strBookingId, string strBookingSegmentId, string strPassengerId, string strBoarding, bool boardFlag, string strToken)
        {
            Library objLi = new Library();
            Helper hlper = new Helper();
            Users users = new Users();
            String strResult = String.Empty;
            String strGetPassengerDetailXML = String.Empty;

            DataSet ds = new DataSet();
            APIErrors errors = new APIErrors();
            bool isParameterWellForm = true;
            string parametersForLog = string.Empty;
            string codeMessage = "907";
            String hky = String.Empty;

            if (System.Configuration.ConfigurationManager.AppSettings["hashkey"] != null)
            {
                hky = System.Configuration.ConfigurationManager.AppSettings["hashkey"].ToString();
            }
            try
            {
                String temp = StringCipher.Decrypt(strToken, hky);

                if (hlper.IsValid(temp))
                {
                    if (string.IsNullOrEmpty(strBookingId))
                    {
                        isParameterWellForm = false;
                        GetAPIErrors("906", errors);
                        LogHelper.writeToLogFile("BoardPassengers", parametersForLog, "906", "Invalid BookingID parameter", string.Empty);
                    }
                    else if (string.IsNullOrEmpty(strBookingSegmentId))
                    {
                        isParameterWellForm = false;
                        GetAPIErrors("310", errors);
                        LogHelper.writeToLogFile("BoardPassengers", parametersForLog, "310", "Invalid bookingsegmentid parameter", string.Empty);
                    }
                    else if (string.IsNullOrEmpty(strPassengerId))
                    {
                        isParameterWellForm = false;
                        GetAPIErrors("312", errors);
                        LogHelper.writeToLogFile("BoardPassengers", parametersForLog, "312", "Invalid passengerid parameter", string.Empty);
                    }
                    else if (string.IsNullOrEmpty(strBoarding))
                    {
                        isParameterWellForm = false;
                        GetAPIErrors("900", errors);
                        LogHelper.writeToLogFile("BoardPassengers", parametersForLog, "900", "Invalid sequence or seat parameter", string.Empty);
                    }

                    if (isParameterWellForm)
                    {
                        try
                        {
                            String xmlBooking = String.Empty;
                            InitializeService();
                            ServiceClient objClient = new ServiceClient();
                            objClient.objService = (TikAeroXMLwebservice)Session["AgentService"];


                            Guid guidBooking = new Guid(strBookingId);
                            xmlBooking = objClient.GetBooking(guidBooking);

                            XmlDocument xmldoc = new XmlDocument();
                            xmldoc.LoadXml(xmlBooking);
                            XPathDocument xmlDoc = new XPathDocument(new StringReader(xmlBooking));
                            XPathNavigator nv = xmlDoc.CreateNavigator();

                            if (xmlBooking != "<Booking />")
                            {
                                string userId = temp;
                                string lockName = xmldoc.SelectSingleNode("Booking/BookingHeader/lock_name").InnerText;

                                if (lockName.Trim().Equals(""))
                                {
                                    foreach (XPathNavigator f in nv.Select("Booking/FlightSegment"))
                                    {
                                        if (strBookingSegmentId.ToLower().Equals(objLi.getXPathNodevalue(f, "booking_segment_id", Library.xmlReturnType.value).ToLower()))
                                        {
                                            if (objLi.getXPathNodevalue(f, "flight_check_in_status_rcd", Library.xmlReturnType.value) == "OPEN" || objLi.getXPathNodevalue(f, "flight_check_in_status_rcd", Library.xmlReturnType.value) == "CLOSED")
                                            {
                                                foreach (XPathNavigator n in nv.Select("Booking/Mapping"))
                                                {
                                                    if (strPassengerId.ToLower().Equals(objLi.getXPathNodevalue(n, "passenger_id", Library.xmlReturnType.value).ToLower()) && objLi.getXPathNodevalue(f, "booking_segment_id", Library.xmlReturnType.value).ToLower().Equals(objLi.getXPathNodevalue(n, "booking_segment_id", Library.xmlReturnType.value).ToLower()))
                                                    {
                                                        String flightID = objLi.getXPathNodevalue(n, "flight_id", Library.xmlReturnType.value);
                                                        String originRCD = objLi.getXPathNodevalue(n, "origin_rcd", Library.xmlReturnType.value);
                                                        if (boardFlag)
                                                        {

                                                            if (objLi.getXPathNodevalue(n, "passenger_check_in_status_rcd", Library.xmlReturnType.value) == "CHECKED")
                                                            {
                                                                strResult = objClient.BoardPassenger(flightID, originRCD, strBoarding, userId, boardFlag);

                                                                if (strResult.Equals("0"))
                                                                {
                                                                    codeMessage = "000";
                                                                }
                                                                else
                                                                {
                                                                    codeMessage = "904";
                                                                }
                                                            }
                                                            else
                                                            {
                                                                codeMessage = "901";
                                                                LogHelper.writeToLogFile("GetBoardingPasses", parametersForLog, "901", "Passenger check in status is not CHECKED", string.Empty);
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (objLi.getXPathNodevalue(n, "passenger_check_in_status_rcd", Library.xmlReturnType.value) == "BOARDED")
                                                            {
                                                                strResult = objClient.BoardPassenger(flightID, originRCD, strBoarding, userId, boardFlag);

                                                                if (strResult.Equals("1"))
                                                                {
                                                                    codeMessage = "000";
                                                                }
                                                                else
                                                                {
                                                                    codeMessage = "905";
                                                                }
                                                            }
                                                            else
                                                            {
                                                                codeMessage = "902";
                                                                LogHelper.writeToLogFile("GetBoardingPasses", parametersForLog, "902", "Passenger check in status is not BOARDED", string.Empty);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                codeMessage = "909";
                                                LogHelper.writeToLogFile("GetBoardingPasses", parametersForLog, "909", "Flight not open or closed for board or un-board", string.Empty);
                                            }
                                        }

                                    }
                                }
                                else
                                {
                                    codeMessage = "911";
                                    LogHelper.writeToLogFile("GetBoardingPasses", parametersForLog, "911", "Booking is in use on AVANTIK", string.Empty);
                                }
                            }
                            else
                            {
                                codeMessage = "908";
                                LogHelper.writeToLogFile("GetBoardingPasses", parametersForLog, "908", "Booking not found", string.Empty);
                            }

                            if (codeMessage != "000")
                            {
                                LogHelper.writeToLogFile("BoardPassengers", parametersForLog, "903", "Can not Baord or Un-Board passenger", string.Empty);
                            }
                            GetAPIErrors(codeMessage, errors);
                        }
                        catch (Exception ex)
                        {
                            GetAPIErrors("907", errors);
                            LogHelper.writeToLogFile("GetBoardingPasses", parametersForLog, "907", ex.Message, ex.StackTrace);
                        }
                    }
                }
                else
                {
                    GetAPIErrors("910", errors);
                    LogHelper.writeToLogFile("GetBoardingPasses", parametersForLog, "910", "Invalid token", string.Empty);
                }
            }
            catch
            {
                GetAPIErrors("910", errors);
                LogHelper.writeToLogFile("GetBoardingPasses", parametersForLog, "910", "Invalid token", string.Empty);
            }
            return objLi.BuildAPIResultXML(errors);
        }

        [WebMethod(EnableSession = true, Description = "OffLoad Passenger")]
        public APIResultMessage OffLoadPassenger(string strBookingId, string strFlightId, string strPassengerId, bool BaggageFlag, string strToken)
        {
            Library objLi = new Library();
            Helper hlper = new Helper();
            Users users = new Users();
            String strResult = String.Empty;
            String strGetPassengerDetailXML = String.Empty;
            strBookingId = strBookingId.ToLower();
            strFlightId = strFlightId.ToLower();
            strPassengerId = strPassengerId.ToLower();

            DataSet ds = new DataSet();
            APIErrors errors = new APIErrors();
            bool isParameterWellForm = true;
            string parametersForLog = string.Empty;
            string codeMessage = "956";
            String hky = String.Empty;

            if (System.Configuration.ConfigurationManager.AppSettings["hashkey"] != null)
            {
                hky = System.Configuration.ConfigurationManager.AppSettings["hashkey"].ToString();
            }
            try
            {
                String temp = StringCipher.Decrypt(strToken, hky);

                if (hlper.IsValid(temp))
                {
                    if (string.IsNullOrEmpty(strBookingId))
                    {
                        isParameterWellForm = false;
                        GetAPIErrors("906", errors);
                        LogHelper.writeToLogFile("OffLoadPassenger", parametersForLog, "906", "Invalid BookingID parameter", string.Empty);
                    }
                    else if (string.IsNullOrEmpty(strFlightId))
                    {
                        isParameterWellForm = false;
                        GetAPIErrors("712", errors);
                        LogHelper.writeToLogFile("OffLoadPassenger", parametersForLog, "712", "FlightID is empty", string.Empty);
                    }
                    else if (string.IsNullOrEmpty(strPassengerId))
                    {
                        isParameterWellForm = false;
                        GetAPIErrors("312", errors);
                        LogHelper.writeToLogFile("OffLoadPassenger", parametersForLog, "312", "Invalid PassengerID parameter", string.Empty);
                    }

                    if (isParameterWellForm)
                    {
                        try
                        {
                            String xmlBooking = String.Empty;
                            InitializeService();
                            ServiceClient objClient = new ServiceClient();
                            objClient.objService = (TikAeroXMLwebservice)Session["AgentService"];


                            Guid guidBooking = new Guid(strBookingId);
                            xmlBooking = objClient.GetBooking(guidBooking);

                            XmlDocument xmldoc = new XmlDocument();
                            xmldoc.LoadXml(xmlBooking);
                            XPathDocument xmlDoc = new XPathDocument(new StringReader(xmlBooking));
                            XPathNavigator nv = xmlDoc.CreateNavigator();

                            if (xmlBooking != "<Booking />")
                            {
                                string userId = temp;
                                string lockName = xmldoc.SelectSingleNode("Booking/BookingHeader/lock_name").InnerText;

                                if (lockName.Trim().Equals(""))
                                {
                                    foreach (XPathNavigator f in nv.Select("Booking/FlightSegment"))
                                    {
                                        if (objLi.getXPathNodevalue(f, "flight_check_in_status_rcd", Library.xmlReturnType.value) == "OPEN" || objLi.getXPathNodevalue(f, "flight_check_in_status_rcd", Library.xmlReturnType.value) == "CLOSED")
                                        {
                                            foreach (XPathNavigator n in nv.Select("Booking/Mapping"))
                                            {
                                                if (strPassengerId.Equals(objLi.getXPathNodevalue(n, "passenger_id", Library.xmlReturnType.value)) && strFlightId.Equals(objLi.getXPathNodevalue(n, "flight_id", Library.xmlReturnType.value)))
                                                {
                                                    strResult = objClient.OffLoadPassenger(strBookingId, strFlightId, strPassengerId, BaggageFlag, userId);

                                                    if (strResult.Equals("True"))
                                                    {
                                                        codeMessage = "000";
                                                    }
                                                    else
                                                    {
                                                        codeMessage = "956";
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            codeMessage = "955";
                                            LogHelper.writeToLogFile("GetBoardingPasses", parametersForLog, "955", "Flight not open or closed for offload", string.Empty);
                                        }

                                    }
                                }
                                else
                                {
                                    codeMessage = "911";
                                    LogHelper.writeToLogFile("OffloadPassenger", parametersForLog, "911", "Booking is in use on AVANTIK", string.Empty);
                                }
                            }
                            else
                            {
                                codeMessage = "908";
                                LogHelper.writeToLogFile("OffloadPassenger", parametersForLog, "908", "Booking not found", string.Empty);
                            }

                            if (codeMessage != "True")
                            {
                                LogHelper.writeToLogFile("OffloadPassenger", parametersForLog, "956", "Can not Offload passenger", string.Empty);
                            }
                            GetAPIErrors(codeMessage, errors);
                        }
                        catch (Exception ex)
                        {
                            GetAPIErrors("956", errors);
                            LogHelper.writeToLogFile("OffloadPassenger", parametersForLog, "956", ex.Message, ex.StackTrace);
                        }
                    }
                }
                else
                {
                    GetAPIErrors("910", errors);
                    LogHelper.writeToLogFile("OffloadPassenger", parametersForLog, "910", "Invalid token", string.Empty);
                }
            }
            catch
            {
                GetAPIErrors("956", errors);
                LogHelper.writeToLogFile("OffloadPassenger", parametersForLog, "956", "Can not offload passenger", string.Empty);
            }
            return objLi.BuildAPIResultXML(errors);
        }

        [WebMethod(EnableSession = true, Description = "Step 3 : Assign seat for passenger")]
        public APIResult AssignSeats(string strBookingSegmentId, string[] strPassengerIds)
        {
            // fix case sensitive
            if (!string.IsNullOrEmpty(strBookingSegmentId))
                strBookingSegmentId = strBookingSegmentId.ToLower();

            if (strPassengerIds != null && strPassengerIds.Length > 0)
            {
                for (int i = 0; i < strPassengerIds.Length; i++)
                {
                    strPassengerIds[i] = strPassengerIds[i].ToLower();
                }
            }

            Mappings objMappings = new Mappings();
            objMappings = (Mappings)Session["Mappings"];

            Passengers paxs = new Passengers();
            paxs = (Passengers)Session["Passengers"];

            Mappings objSaveMapping = new Mappings();
            CheckInPassengers objCki = new CheckInPassengers();
            APIPassengerMappings mappings = new APIPassengerMappings();
            APIErrors errors = new APIErrors();
            Library objLi = new Library();
            DataSet ds = new DataSet();

            bool b = false;
            bool isParameterWellForm = true;
            string strGetPassengerDetailXML = string.Empty;
            string strPassengerIdsForXML = string.Empty;
            bool isContainAdult = false;
            int iAdult = 0;
            int iChild = 0;
            int iInfant = 0;
            string strNotInfant = string.Empty;
            string strInfant = string.Empty;
            string[] strNotInfants = null;
            string[] strInfants = null;
            bool isAllowed = false;
            bool isIncludeOFFLOADED = false;
            string message = "301";
            bool isIncludeSeatAssigned = false;
            string parametersForLog = string.Empty;

            parametersForLog += "strBookingSegmentId : " + strBookingSegmentId + "::: passengerId :";
            foreach (string passengerId in strPassengerIds)
            {
                parametersForLog += passengerId + "|";
            }

            if (string.IsNullOrEmpty(strBookingSegmentId))
            {
                isParameterWellForm = false;
                GetAPIErrors("310", errors);
                LogHelper.writeToLogFile("AssignSeats", parametersForLog, "310", "BookingSegmentId is empty", string.Empty);
            }
            else if (strPassengerIds == null || strPassengerIds.Length == 0)
            {
                isParameterWellForm = false;
                GetAPIErrors("312", errors);
                LogHelper.writeToLogFile("AssignSeats", parametersForLog, "312", "PassengerIds is empty", string.Empty);
            }

            if (isParameterWellForm)
            {
                try
                {
                    if (objMappings != null && objMappings.Count > 0)
                    {
                        //check passenger type & OFFLOADED for seat assign from web config
                        string strPassengerTypeBlock = string.Empty;
                        bool isSeatOFFLOADEDAllowed = false;
                        bool isAllowCHDCheckinAlone = false;

                        if (System.Configuration.ConfigurationManager.AppSettings["SeatPassengerTypeBlock"] != null)
                        {
                            strPassengerTypeBlock = System.Configuration.ConfigurationManager.AppSettings["SeatPassengerTypeBlock"].ToUpper();
                        }
                        if (System.Configuration.ConfigurationManager.AppSettings["SeatOFFLOADEDAllowed"] != null)
                        {
                            isSeatOFFLOADEDAllowed = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["SeatOFFLOADEDAllowed"]);
                        }
                        if (System.Configuration.ConfigurationManager.AppSettings["isAllowCHDCheckinAlone"] != null)
                        {
                            isAllowCHDCheckinAlone = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["isAllowCHDCheckinAlone"]);
                        }

                        //check passenger condition -- not allow CHD and INF checkin independently
                        strGetPassengerDetailXML = GetFlightInformation(strBookingSegmentId);
                        XPathDocument xmlDoc = new XPathDocument(new StringReader(strGetPassengerDetailXML));
                        XPathNavigator nv = xmlDoc.CreateNavigator();

                        foreach (string strPassengerId in strPassengerIds)
                        {
                            foreach (Mapping m in objMappings)
                            {
                                if (m.booking_segment_id.Equals(new Guid(strBookingSegmentId)) &&
                                   (strPassengerId.Length == 0 || m.passenger_id.Equals(new Guid(strPassengerId))) &&
                                    !strPassengerTypeBlock.Contains(m.passenger_type_rcd.ToUpper()))
                                    //&& string.IsNullOrEmpty(m.seat_number))
                                {
                                    if (m.e_ticket_flag == 1 && m.ticket_number.Length > 0)
                                    {
                                        if (m.passenger_check_in_status_rcd == null || m.passenger_check_in_status_rcd.Length == 0)
                                        {
                                            isAllowed = true;
                                        }

                                        //yo 31-01
                                        if (m.passenger_check_in_status_rcd == "OFFLOADED")
                                        {
                                            if (!isSeatOFFLOADEDAllowed)
                                            {
                                                isIncludeOFFLOADED = true;
                                            }
                                            else
                                            {
                                                isAllowed = true;
                                            }
                                        }

                                        //yo 10-02
                                        if (!string.IsNullOrEmpty(m.seat_number))
                                        {
                                            isIncludeSeatAssigned = true;
                                        }

                                        if (isAllowed)
                                        {
                                            switch (m.passenger_type_rcd.ToUpper())
                                            {
                                                    //case "ADULT":
                                                    //isContainAdult = true;
                                                    //if (m.free_seating_flag != 1)//yo 30-01
                                                    //{
                                                    //    strNotInfant += strPassengerId + "|";
                                                    //}
                                                    //iAdult++;
                                                   // break;
                                                case "CHD":
                                                    if (m.free_seating_flag != 1)//yo 30-01
                                                    {
                                                        strNotInfant += strPassengerId + "|";
                                                    }
                                                    iChild++;
                                                    break;
                                                case "INF":
                                                    if (m.free_seating_flag != 1)//yo 30-01
                                                    {
                                                        strInfant += strPassengerId + "|";
                                                    }
                                                    iInfant++;
                                                    break;
                                                default:
                                                    isContainAdult = true;
                                                    if (m.free_seating_flag != 1)//yo 30-01
                                                    {
                                                        strNotInfant += strPassengerId + "|";
                                                    }
                                                    iAdult++;

                                                    break;
                                            }

                                            objSaveMapping.Add(m); 
                                        }

                                    }
                                    strPassengerIdsForXML += m.passenger_id + "|";
                                }
                            }
                        }
                        Session["SaveMapping"] = objSaveMapping;
                        
                        if (isIncludeOFFLOADED)//!isAllowed
                        {
                            GetAPIErrors("305", errors); //OFFLOADED passenger can't checkin
                            LogHelper.writeToLogFile("AssignSeats", parametersForLog, "305", "OFFLOADED passenger can't checkin", string.Empty);
                        }
                        //else if (iAdult+iChild+iInfant == 0)
                        //{
                        //    GetAPIErrors("313", errors); //Passengers could not be zero
                        //    LogHelper.writeToLogFile("AssignSeats", parametersForLog, "313", "Passengers could not be zero", string.Empty);
                        //}
                        else if (isIncludeSeatAssigned)
                        {
                            GetAPIErrors("314", errors); //Passengers already have been assigned seat
                            LogHelper.writeToLogFile("AssignSeats", parametersForLog, "314", "Passengers already have been assigned seat", string.Empty);
                        }
                        else if (!isContainAdult && !isAllowCHDCheckinAlone)
                        {
                            GetAPIErrors("303", errors); //Child or infant can't assign seat independently
                            LogHelper.writeToLogFile("AssignSeats", parametersForLog, "303", "Child or infant can't assign seat independently", string.Empty);
                        }
                        else if (iInfant > iAdult)
                        {
                            GetAPIErrors("304", errors); //Number of adults need to be higher than the number of infants
                            LogHelper.writeToLogFile("AssignSeats", parametersForLog, "304", "Number of adults need to be higher than the number of infants", string.Empty);
                        }
                        else
                        {
                            strNotInfants = strNotInfant.Split('|');
                            strInfants = strInfant.Split('|');

                            //Session["SaveMapping"] = objSaveMapping;
                            if (objSaveMapping.Count > 0 && objSaveMapping[0].free_seating_flag == 0)
                            {
                                //check allpaxhaveseat
                                bool allHaveSeat = false;
                                bool paxHaveSeat = false;
                                foreach (Mapping mp in objSaveMapping)
                                {
                                    paxHaveSeat = objCki.AllPaxHaveSeat(objSaveMapping, mp.booking_segment_id, mp.passenger_id.ToString());
                                    if (paxHaveSeat)
                                    {
                                        allHaveSeat = true;
                                    }
                                }

                                if (!allHaveSeat)
                                {
                                    Itinerary it = (Itinerary)Session["Itinerary"];

                                    ServiceClient objClient = new ServiceClient();
                                    objClient.objService = (TikAeroXMLwebservice)Session["AgentService"];

                                    //Load Seat map.
                                    //ds = objClient.GetSeatMap(it[0].origin_rcd, it[0].destination_rcd, it[0].flight_id.ToString(), it[0].boarding_class_rcd, it[0].booking_class_rcd, tikAEROWebCheckinAPI.Classes.Language.CurrentCode());
                                    ds = objClient.GetSeatMapLayout(it[0].flight_id.ToString(), it[0].origin_rcd, it[0].destination_rcd, it[0].boarding_class_rcd, string.Empty, tikAEROWebCheckinAPI.Classes.Language.CurrentCode());
                                    if (ds != null && ds.Tables.Count > 0)
                                    {
                                        switch (ConfigurationManager.AppSettings["SeatAssignType"].ToUpper())
                                        {
                                            case "P":
                                                b = objCki.AssignSeatPercentage(ds.Tables[0], ref objSaveMapping, ref message, false, false);
                                                break;
                                            case "BB":
                                                b = objCki.AssignSeatByBay(ds.Tables[0], ref objSaveMapping, ref message, false, false);
                                                break;
                                            case "FB":
                                                b = objCki.AssignSeatFromBack(ds.Tables[0], ref objSaveMapping, ref message, false, false);
                                                break;
                                            case "M":

                                                b = objCki.AssignSeatFromMiddle(ds.Tables[0], ref objSaveMapping, message, false, false);
                                                //b = objCki.AssignSeatFromMiddle(ds.Tables[0], ref objSaveMapping);
                                                break;
                                            default:
                                                b = false;
                                                break;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                message = "307";//free_seating_flag = 1;This flight is free-seating.
                            }

                            //add log
                            if (objSaveMapping != null)
                            {
                                
                                foreach (Mapping m in objSaveMapping)
                                {
                                    parametersForLog += "booking id : "+ m.booking_id +" | pax id : " + m.passenger_id + " | seat number : "+ m.seat_number + "|";
                                }
                                LogHelper.writeToLogFile("AssignSeats", parametersForLog, "", "Log for assign seat", string.Empty);
                                Session["SaveMapping"] = objSaveMapping;
                            }

                            if (b)
                            {
                                strGetPassengerDetailXML = GetFlightInformation(strBookingSegmentId);

                                string[] passIDs = strPassengerIdsForXML.Split('|');
                                if (passIDs.Length > 0)
                                {
                                    foreach (string passID in passIDs)
                                    {
                                        if (passID != "")
                                        {
                                            GetAPIPassengerMappings(strBookingSegmentId, passID, strGetPassengerDetailXML, mappings);
                                        }
                                    }
                                }
                                GetAPIErrors("000", errors);
                            }
                            else
                            {
                                //GetAPIErrors("301", errors);
                                GetAPIErrors(message, errors);//301

                                if (message == "301")
                                {
                                    LogHelper.writeToLogFile("AssignSeats", parametersForLog, "301", "Passengers could not be assigned seat", string.Empty);
                                }
                                else if (message == "307")
                                {
                                    LogHelper.writeToLogFile("AssignSeats", parametersForLog, "307", "This flight is free-seating", string.Empty);
                                }
                            }

                        }
                    }
                    else
                    {
                        GetAPIErrors("302", errors);
                        LogHelper.writeToLogFile("AssignSeats", parametersForLog, "302", "objMappings is null", string.Empty);
                    }
                }
                catch (Exception ex)
                {
                    GetAPIErrors("300", errors);
                    LogHelper.writeToLogFile("AssignSeats", parametersForLog, "300", ex.Message, ex.StackTrace);
                }
            }
            return objLi.BuildAPIResultXML(null, mappings, null, null, null, null, null, errors);
        }

        [WebMethod(EnableSession = true, Description = "Step 4 : Check in")]
        public APIResult Commit(string strBookingSegmentId, string[] strPassengerIds)
        {
            // fix case sensitive
            if (!string.IsNullOrEmpty(strBookingSegmentId))
                strBookingSegmentId = strBookingSegmentId.ToLower();

            if (strPassengerIds != null && strPassengerIds.Length > 0)
            {
                for (int i = 0; i < strPassengerIds.Length; i++)
                {
                    strPassengerIds[i] = strPassengerIds[i].ToLower();
                }
            }

            Mappings objSaveMapping = new Mappings();
            objSaveMapping = (Mappings)Session["SaveMapping"];
            
            Mappings objMappings = new Mappings();
            objMappings = (Mappings)Session["Mappings"];
            
            Passengers paxs = new Passengers();
            paxs = (Passengers)Session["Passengers"];
            
            CheckInPassengers objCki = new CheckInPassengers();
            APIPassengerMappings mappings = new APIPassengerMappings();
            APIResult result = new APIResult();
            APIErrors errors = new APIErrors();
            Library objLi = new Library();

            bool bResult = false;
            bool isParameterWellForm = true;
            bool isContainAdult = false;
            int iAdult = 0;
            int iChild = 0;
            int iInfant = 0;
            int iAttempt = 0;
            bool isAllowed = false;
            bool isNeedSeatAssign = false;
            bool isIncludeOFFLOADED = false;
            bool isIncludeNotAllowed = false;
            bool isInObjSaveMapping = false;
            string parametersForLog = string.Empty;
            string strPassengerIdsForXML = string.Empty;
            string strGetPassengerDetailXML = string.Empty;

            parametersForLog += "strBookingSegmentId : " + strBookingSegmentId + ":::passengerId :";
            foreach (string passengerId in strPassengerIds)
            {
                parametersForLog += passengerId + "|";
            }

            if (string.IsNullOrEmpty(strBookingSegmentId))
            {
                isParameterWellForm = false;
                GetAPIErrors("410", errors);
                LogHelper.writeToLogFile("Commit", parametersForLog, "410", "BookingSegmentId is empty", string.Empty);
            }
            else if (strPassengerIds == null || strPassengerIds.Length == 0)
            {
                isParameterWellForm = false;
                GetAPIErrors("411", errors);
                LogHelper.writeToLogFile("Commit", parametersForLog, "411", "PassengerIds is empty", string.Empty);
            }

            if (isParameterWellForm)
            {
                try
                {
                    if (objMappings != null)
                    {

                        //check passenger type & OFFLOADED for seat assign from web config
                        string strPassengerTypeBlock = string.Empty;
                        bool isSeatOFFLOADEDAllowed = false;
                        bool isAllowCHDCheckinAlone = false;

                        if (System.Configuration.ConfigurationManager.AppSettings["SeatPassengerTypeBlock"] != null)
                        {
                            strPassengerTypeBlock = System.Configuration.ConfigurationManager.AppSettings["SeatPassengerTypeBlock"].ToUpper();
                        }
                        if (System.Configuration.ConfigurationManager.AppSettings["SeatOFFLOADEDAllowed"] != null)
                        {
                            isSeatOFFLOADEDAllowed = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["SeatOFFLOADEDAllowed"]);
                        }
                        if (System.Configuration.ConfigurationManager.AppSettings["isAllowCHDCheckinAlone"] != null)
                        {
                            isAllowCHDCheckinAlone = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["isAllowCHDCheckinAlone"]);
                        }

                        //check passenger condition -- not allow CHD and INF checkin independently
                        strGetPassengerDetailXML = GetFlightInformation(strBookingSegmentId);

                        XPathDocument xmlDoc = new XPathDocument(new StringReader(strGetPassengerDetailXML));
                        XPathNavigator nv = xmlDoc.CreateNavigator();

                        foreach (string strPassengerId in strPassengerIds)
                        {
                            //check passenger condition -- not allow CHD and INF checkin independently
                            foreach (XPathNavigator n2 in nv.Select("Booking/Passenger[passenger_id='" + strPassengerId + "']"))
                            {
                                switch (XmlHelper.XpathValueNullToEmpty(n2, "passenger_type_rcd").ToUpper())
                                {
                                        //case "ADULT" :
                                        //isContainAdult = true;
                                        //iAdult++;
                                       // break;
                                    case "CHD" :
                                        iChild++;
                                        break;
                                    case "INF" :
                                        iInfant++;
                                        break;
                                    default :
                                        isContainAdult = true;
                                        iAdult++;
                                        break;
                                }
                            }

                            if (objSaveMapping != null)
                            {
                                foreach (Mapping m in objSaveMapping)
                                {
                                    if (m.booking_segment_id.Equals(new Guid(strBookingSegmentId)) &&
                                       (strPassengerId.Length == 0 || m.passenger_id.Equals(new Guid(strPassengerId))) &&
                                        !strPassengerTypeBlock.Contains(m.passenger_type_rcd.ToUpper()))
                                    {
                                        if (m.e_ticket_flag == 1 && m.ticket_number.Length > 0)
                                        {

                                            if (m.passenger_check_in_status_rcd == null || m.passenger_check_in_status_rcd.Length == 0)
                                            {
                                                isAllowed = true;
                                            }

                                            //yo 30-01
                                            if (m.free_seating_flag == 0 && string.IsNullOrEmpty(m.seat_number))
                                            {
                                                isNeedSeatAssign = true;
                                            }
                                            else
                                            {
                                                parametersForLog += "booking id : " + m.booking_id + " | pax id : " + m.passenger_id + " | seat number : " + m.seat_number + "|";
                                            }

                                            //yo 31-01
                                            if (m.passenger_check_in_status_rcd == "OFFLOADED")
                                            {
                                                if (!isSeatOFFLOADEDAllowed)
                                                {
                                                    isIncludeOFFLOADED = true;
                                                }
                                                else
                                                {
                                                    isAllowed = true;
                                                }
                                            }

                                            if (isAllowed && isNeedSeatAssign == false)
                                            {
                                                m.passenger_check_in_status_rcd = "CHECKED";
                                                m.passenger_status_rcd = "OK";
                                                m.standby_flag = 0;
                                                m.check_in_user_code = "WEB";
                                                m.check_in_by = m.passenger_id;
                                                m.check_in_code = "WEB";
                                                m.check_in_date_time = DateTime.Today;

                                                if (m.hand_luggage_flag == 1)
                                                {
                                                    m.hand_number_of_pieces = paxs.Count;
                                                    m.hand_baggage_weight = paxs.Count + Convert.ToDouble(ConfigurationManager.AppSettings["HandWeight"]);
                                                }
                                                //objSaveMapping.Add(m);
                                            }
                                            else
                                            {
                                                //yo 09-02
                                                isIncludeNotAllowed = true;
                                            }

                                            //yo 13-02
                                            isInObjSaveMapping = true;
                                        }

                                        strPassengerIdsForXML += m.passenger_id + "|";
                                    }

                                    if (isIncludeNotAllowed) break;
                                }
                            }

                            //passsengerId not in objSaveMapping //yo 13-02
                            if (!isInObjSaveMapping)
                            {
                                //find again in objMappings
                                foreach (Mapping mm in objMappings)
                                {
                                    if (mm.booking_segment_id.Equals(new Guid(strBookingSegmentId)) &&
                                   (strPassengerId.Length == 0 || mm.passenger_id.Equals(new Guid(strPassengerId))) &&
                                    !strPassengerTypeBlock.Contains(mm.passenger_type_rcd.ToUpper()))
                                    {
                                        if (mm.e_ticket_flag == 1 && mm.ticket_number.Length > 0)
                                        {

                                            if (mm.passenger_check_in_status_rcd == null || mm.passenger_check_in_status_rcd.Length == 0)
                                            {
                                                isAllowed = true;
                                            }

                                            if (mm.free_seating_flag == 0 && string.IsNullOrEmpty(mm.seat_number))
                                            {
                                                isNeedSeatAssign = true;
                                            }
                                            else
                                            {
                                                parametersForLog += "booking id : " + mm.booking_id + " | pax id : " + mm.passenger_id + " | seat number : " + mm.seat_number + "|";
                                            }

                                            if (mm.passenger_check_in_status_rcd == "OFFLOADED")
                                            {
                                                if (!isSeatOFFLOADEDAllowed)
                                                {
                                                    isIncludeOFFLOADED = true;
                                                }
                                                else
                                                {
                                                    isAllowed = true;
                                                }
                                            }

                                            if (isAllowed && isNeedSeatAssign == false)
                                            {
                                                mm.passenger_check_in_status_rcd = "CHECKED";
                                                mm.passenger_status_rcd = "OK";
                                                mm.standby_flag = 0;
                                                mm.check_in_user_code = "WEB";
                                                mm.check_in_by = mm.passenger_id;
                                                mm.check_in_code = "WEB";
                                                mm.check_in_date_time = DateTime.Today;

                                                if (mm.hand_luggage_flag == 1)
                                                {
                                                    mm.hand_number_of_pieces = paxs.Count;
                                                    mm.hand_baggage_weight = paxs.Count + Convert.ToDouble(ConfigurationManager.AppSettings["HandWeight"]);
                                                }

                                                if (objSaveMapping == null)
                                                {
                                                    objSaveMapping = new Mappings();
                                                }

                                                objSaveMapping.Add(mm);
                                            }
                                            else
                                            {
                                                isIncludeNotAllowed = true;
                                            }
                                        }

                                        strPassengerIdsForXML += mm.passenger_id + "|";
                                    }

                                    if (isIncludeNotAllowed) break;
                                }
                            }

                            //checking on checkin status of updated objMapping retrieved from session 18-02-2013
                            objMappings = (Mappings)Session["Mappings"];

                            //foreach (Mapping mm in objMappings)
                            //{
                            //    if (mm.booking_segment_id.Equals(new Guid(strBookingSegmentId)) &&
                            //   (mm.passenger_id.Equals(new Guid(strPassengerId))) &&
                            //    !strPassengerTypeBlock.Contains(mm.passenger_type_rcd.ToUpper()))
                            //    {
                            //        if (mm.passenger_check_in_status_rcd == "CHECKED")
                            //        {
                            //            isIncludeNotAllowed = true;
                            //        }
                            //    }
                            //}
                            

                            isInObjSaveMapping = false;
                            //end check passsengerId not in objSaveMapping

                            if (isIncludeNotAllowed) break;
                        }

                        if (isIncludeOFFLOADED)//!IsAllowed
                        {
                            GetAPIErrors("405", errors); //OFFLOADED passenger can't checkin
                            LogHelper.writeToLogFile("Commit", parametersForLog, "405", "OFFLOADED passenger can't checkin", string.Empty);
                        }
                        else if (iAdult + iChild + iInfant == 0 )
                        {
                            GetAPIErrors("406", errors); 
                            LogHelper.writeToLogFile("Commit", parametersForLog, "406", "Number of passengers could not be zero", string.Empty);
                        }
                        else if (isIncludeNotAllowed)
                        {
                            GetAPIErrors("407", errors);
                            LogHelper.writeToLogFile("Commit", parametersForLog, "407", "Invalid passenger check in condition", string.Empty);
                        }
                        else if (!isContainAdult && !isAllowCHDCheckinAlone)
                        {
                            GetAPIErrors("403", errors); //Child or infant can't checkin independently
                            LogHelper.writeToLogFile("Commit", parametersForLog, "403", "Child or infant can't checkin independently", string.Empty);
                        }
                        else if (iInfant > iAdult)
                        {
                            GetAPIErrors("404", errors); //Number of adults need to be higher than the number of infants
                            LogHelper.writeToLogFile("Commit", parametersForLog, "404", "Number of adults need to be higher than the number of infants", string.Empty);
                        }
                        else if (isNeedSeatAssign)
                        {
                            GetAPIErrors("402", errors); //Required assign seat before check in.
                            LogHelper.writeToLogFile("Commit", parametersForLog, "402", "Required assign seat before check in", string.Empty);
                        }
                        else
                        {
                            LogHelper.writeToLogFile("Commit", parametersForLog, "", "Log for commit", string.Empty);

                            objCki.objService = (TikAeroXMLwebservice)Session["AgentService"];
                            
                            bResult = objCki.CheckInSave(XmlHelper.Serialize(objSaveMapping, false),
                                                string.Empty,
                                                string.Empty,
                                                string.Empty,
                                                string.Empty,
                                                string.Empty,
                                                string.Empty,
                                                string.Empty);

                            if (bResult)
                            {
                                //Reload Booking information.
                                Session.Remove("Mappings");
                                Session.Remove("Itinerary");
                                Session.Remove("Passengers");

                                strGetPassengerDetailXML = GetFlightInformation(strBookingSegmentId);

                                string[] passIDs = strPassengerIdsForXML.Split('|');
                                if (passIDs.Length > 0)
                                {
                                    foreach (string passID in passIDs)
                                    {
                                        if (passID != "")
                                        {
                                            GetAPIPassengerMappings(strBookingSegmentId, passID, strGetPassengerDetailXML, mappings);
                                        }
                                    }
                                }
                                GetAPIErrors("000", errors);
                            }
                            else
                            {

                                //if it fails on commit then try to re-assign seat and commit again.
                                if (System.Configuration.ConfigurationManager.AppSettings["attemptCommit"] != null)
                                {
                                    iAttempt = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["attemptCommit"]);

                                    if (IsClearSeat(objMappings))
                                    {
                                        for (int i = 0; i < iAttempt; i++)
                                        {
                                            result = AssignSeats(strBookingSegmentId, strPassengerIds);
                                            if (IsSuccess(result))
                                            {
                                                result = Commit(strBookingSegmentId, strPassengerIds);
                                                if (IsSuccess(result))
                                                {
                                                    GetAPIErrors("000", errors);
                                                    bResult = true;
                                                    break;
                                                }
                                            }
                                            else
                                            {
                                                LogHelper.writeToLogFile("Commit", parametersForLog, "", "Fail re-assign seat on attemp no."+(i+1)+"", string.Empty);
                                            }
                                        }
                                    }                              
                                }

                                if (!bResult)
                                {
                                    GetAPIErrors("401", errors);
                                    LogHelper.writeToLogFile("Commit", parametersForLog, "401", "Passengers could not check in", string.Empty);
                                }

                            }

                        }
                    }
                    else
                    {
                        GetAPIErrors("402", errors);//Required assign seat before check in.
                        LogHelper.writeToLogFile("Commit", parametersForLog, "402", "Required assign seat before check in", string.Empty);
                    }
                }
                catch (Exception ex)
                {
                    GetAPIErrors("400", errors);
                    LogHelper.writeToLogFile("Commit", parametersForLog, "400", ex.Message, ex.StackTrace);
                }
            }
            return objLi.BuildAPIResultXML(null, null, null, null, null, null, null, errors);
        }

        [WebMethod(EnableSession = true, Description = "Step 5 : Get information for boardingpass printing")]
        public APIResult GetBoardingPasses(string strBookingSegmentId, string[] strPassengerIds)
        {
            // fix case sensitive
            if (!string.IsNullOrEmpty(strBookingSegmentId))
                strBookingSegmentId = strBookingSegmentId.ToLower();

            if (strPassengerIds != null && strPassengerIds.Length > 0)
            {
                for (int i = 0; i < strPassengerIds.Length; i++)
                {
                    strPassengerIds[i] = strPassengerIds[i].ToLower();
                }
            }

            Library objLi = new Library();
            Helper objHelper = new Helper();
            CheckInPassengers ckps = new CheckInPassengers();
            APIPassengerMappings mappings = new APIPassengerMappings();
            APIPassengerServices services = new APIPassengerServices();
            APIErrors errors = new APIErrors();
            
            string strGetPassengerDetailXML = string.Empty;
            string codeMessage = "502"; // Required check in before get boarding pass
            string parametersForLog = string.Empty;
            bool isParameterWellForm = true;

            parametersForLog += "strBookingSegmentId : " + strBookingSegmentId + ":::passengerId :";
            foreach (string passengerId in strPassengerIds)
            {
                parametersForLog += passengerId + "|";
            }

            if (string.IsNullOrEmpty(strBookingSegmentId))
            {
                isParameterWellForm = false;
                GetAPIErrors("510", errors);
                LogHelper.writeToLogFile("GetBoardingPasses", parametersForLog, "510", "BookingSegmentId is empty", string.Empty);
            }
            else if (strPassengerIds == null || strPassengerIds.Length == 0)
            {
                isParameterWellForm = false;
                GetAPIErrors("511", errors);
                LogHelper.writeToLogFile("GetBoardingPasses", parametersForLog, "511", "PassengerIds is empty", string.Empty);
            }

            if (isParameterWellForm)
            {
                try
                {
                    if (Session["CheckInFlight"] != null)
                    {
                        objHelper.GetFlightInformation(ref ckps,
                                                        strBookingSegmentId,
                                                        Session["CheckInFlight"].ToString());

                        ckps.objService = (TikAeroXMLwebservice)Session["AgentService"];

                        XPathDocument xmlDoc = new XPathDocument(new StringReader(ckps.GetPassengerDetails("EN")));
                        XPathNavigator nv = xmlDoc.CreateNavigator();
                        //StringBuilder stb = new StringBuilder();

                        strGetPassengerDetailXML = GetFlightInformation(strBookingSegmentId);

                        if (strGetPassengerDetailXML != "<Booking />")
                        {
                            //stb.Append("<Booking>");
                            foreach (string strPassengerId in strPassengerIds)
                            {

                                foreach (XPathNavigator n in nv.Select("Booking/Mapping"))
                                {
                                    if (strPassengerId.Equals(objLi.getXPathNodevalue(n, "passenger_id", Library.xmlReturnType.value)))
                                    {
                                        //stb.Append(n.OuterXml);

                                        if (objLi.getXPathNodevalue(n, "passenger_check_in_status_rcd", Library.xmlReturnType.value) == "CHECKED")
                                        {
                                            GetAPIPassengerMappings(strBookingSegmentId, strPassengerId, strGetPassengerDetailXML, mappings);
                                            GetAPIPassengerServices(strBookingSegmentId, strPassengerId, strGetPassengerDetailXML, services);
                                            codeMessage = "000";
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            codeMessage = "503";
                            LogHelper.writeToLogFile("GetBoardingPasses", parametersForLog, "503", "strGetPassengerDetailXML is empty", string.Empty);
                        }

                        GetAPIErrors(codeMessage, errors);
                        if (codeMessage == "502")
                        {
                            LogHelper.writeToLogFile("GetBoardingPasses", parametersForLog, "502", "Required check in before get boarding pass", string.Empty);
                        }
                    }
                    else
                    {
                        GetAPIErrors("501", errors);
                        LogHelper.writeToLogFile("GetBoardingPasses", parametersForLog, "501", "Session[CheckInFlight] is null", string.Empty);
                    }
                }
                catch (Exception ex)
                {
                    GetAPIErrors("500", errors);
                    LogHelper.writeToLogFile("GetBoardingPasses", parametersForLog, "500", ex.Message, ex.StackTrace);
                }

                //stb.Append("</Booking>");
                //return stb.ToString();
            }
            return objLi.BuildAPIResultXML(null, mappings, null, services, null, null, null, errors);
        }
        
        [WebMethod(EnableSession = true, Description = "Change passenger passport information")]
        public APIResult UpdatePassengerDocumentDetails(APIPassengerUpdateRequests request)
        {
            APIErrors errors = new APIErrors();
            Library objLi = new Library();

            bool bFoundError = false;
            int iResult = 0;
            try
            {
                if (Session["CheckInFlight"] != null)
                {
                    if (request != null)
                        if (request.Count > 0)
                        {
                            Passengers paxs = new Passengers();
                            tikSystem.Web.Library.Passenger pax = null;
                            //Fill in Request object.
                            for (int i = 0; i < request.Count; i++)
                            {
                                pax = new tikSystem.Web.Library.Passenger();
                                if (request[i].passenger_id.Equals(Guid.Empty))
                                {
                                    GetAPIErrors("602", errors);
                                    LogHelper.writeToLogFile("UpdatePassengerDocumentDetails", request[i].passenger_id.ToString(), "602", "PassengerId is empty", string.Empty);
                                    bFoundError = true;
                                }
                                else
                                {
                                    pax.passenger_id = request[i].passenger_id;
                                }

                                if (request[i].date_of_birth.Equals(DateTime.MinValue) == false)
                                {
                                    pax.date_of_birth = request[i].date_of_birth;
                                }

                                if (request[i].passport_expiry_date.Equals(DateTime.MinValue) == false)
                                {
                                    pax.passport_expiry_date = request[i].passport_expiry_date;
                                }

                                pax.gender_type_rcd = request[i].gender_type_rcd.ToUpper();
                                pax.nationality_rcd = request[i].nationality_rcd.ToUpper();
                                pax.passport_issue_country_rcd = request[i].passport_issue_country_rcd.ToUpper();
                                pax.passport_number = request[i].passport_number.ToUpper();

                                //18-04-2013 : add document type and document number
                                pax.document_type_rcd = request[i].document_type_rcd.ToUpper();

                                if (bFoundError == false)
                                { paxs.Add(pax); }
                            }

                            if (bFoundError == false)
                            {
                                ServiceClient objService = new ServiceClient();
                                objService.objService = (TikAeroXMLwebservice)Session["AgentService"];
                                iResult = objService.UpdatePassengerDocumentDetails(paxs);
                                
                                if (iResult == request.Count)
                                {

                                    GetAPIErrors("000", errors);
                                    //Update new information to Mapping session.
                                    Mappings objMappings = (Mappings)Session["Mappings"];
                                    for (int i = 0; i < request.Count; i++)
                                    {
                                        for (int j = 0; j < objMappings.Count; j++)
                                        {
                                            if (request[i].passenger_id.Equals(objMappings[j].passenger_id))
                                            {
                                                objMappings[j].gender_type_rcd = request[i].gender_type_rcd;
                                                objMappings[j].date_of_birth = request[i].date_of_birth;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    GetAPIErrors("600", errors);
                                    LogHelper.writeToLogFile("UpdatePassengerDocumentDetails", "iResult = " + iResult.ToString() + "", "600", "iResult is not equal Request", string.Empty);
                                }
                            }
                        }
                        else
                        {
                            GetAPIErrors("601", errors);
                            LogHelper.writeToLogFile("UpdatePassengerDocumentDetails", "request.Count = " + request.Count.ToString() + "", "601", "Result is empty", string.Empty);
                        }
                }
                else
                {
                    GetAPIErrors("603", errors);
                    LogHelper.writeToLogFile("UpdatePassengerDocumentDetails", "Session[CheckInFlight]", "501", "Session[CheckInFlight] is null", string.Empty);
                }
            }
            catch (Exception ex)
            {
                GetAPIErrors("600", errors);
                LogHelper.writeToLogFile("UpdatePassengerDocumentDetails", "", "500", ex.Message, ex.StackTrace);
            }
            return objLi.BuildAPIResultXML(null, null, null, null, null, null, null, errors);
        }

        [WebMethod(EnableSession = true, Description = "Get seat map information")]
        public APIResult GetSeatMap(string originRcd, string destinationRcd, Guid flightId, string boardingClass, string bookingClass)
        {
            Mappings objMappings = new Mappings();
            objMappings = (Mappings)Session["Mappings"];

            Library objLi = new Library();
            DataSet ds = new DataSet();
            APIErrors errors = new APIErrors();
            APISeatMaps seatmaps = new APISeatMaps();
            bool isParameterWellForm = true;
            string parametersForLog = string.Empty;
            string codeMessage = "715"; // No seat map information found

            if (string.IsNullOrEmpty(originRcd))
            {
                isParameterWellForm = false;
                GetAPIErrors("710", errors);
                LogHelper.writeToLogFile("GetSeatMap", parametersForLog, "710", "Parameter OriginRcd is empty", string.Empty);
            }
            else if (string.IsNullOrEmpty(destinationRcd))
            {
                isParameterWellForm = false;
                GetAPIErrors("711", errors);
                LogHelper.writeToLogFile("GetSeatMap", parametersForLog, "711", "Parameter DestionationRcd is empty", string.Empty);
            }
            else if (flightId.Equals(Guid.Empty))
            {
                isParameterWellForm = false;
                GetAPIErrors("712", errors);
                LogHelper.writeToLogFile("GetSeatMap", parametersForLog, "712", "Parameter FlightId is empty", string.Empty);
            }
            else if (string.IsNullOrEmpty(boardingClass))
            {
                isParameterWellForm = false;
                GetAPIErrors("713", errors);
                LogHelper.writeToLogFile("GetSeatMap", parametersForLog, "713", "Parameter BoardingClass is empty", string.Empty);
            }
            else if (string.IsNullOrEmpty(bookingClass))
            {
                isParameterWellForm = false;
                GetAPIErrors("714", errors);
                LogHelper.writeToLogFile("GetSeatMap", parametersForLog, "714", "Parameter BookingClass is empty", string.Empty);
            }

            if (isParameterWellForm)
            {
                try
                {
                    if (objMappings != null && objMappings.Count > 0)
                    {
                        ServiceClient objClient = new ServiceClient();
                        objClient.objService = (TikAeroXMLwebservice)Session["AgentService"];

                        ds = objClient.GetSeatMap(originRcd, destinationRcd, flightId.ToString(), boardingClass, bookingClass, tikAEROWebCheckinAPI.Classes.Language.CurrentCode());

                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            ds.DataSetName = "SeatMaps";
                            GetAPISeatMaps(ds.Tables[0].Rows[0]["flight_id"].ToString(), ds.GetXml(), seatmaps);
                            codeMessage = "000";
                        }
                        else
                        {
                            codeMessage = "715";
                        }

                        GetAPIErrors(codeMessage, errors);

                        if (codeMessage != "000")
                        {
                            LogHelper.writeToLogFile("GetSeatMap", parametersForLog, "715", "No seat map information found", string.Empty);
                        }
                    }
                    else
                    {
                        GetAPIErrors("701", errors);
                        LogHelper.writeToLogFile("GetSeatMap", parametersForLog, "701", "objMappings is null", string.Empty);
                    }
                }
                catch (Exception ex)
                {
                    GetAPIErrors("700", errors);
                    LogHelper.writeToLogFile("GetBoardingPasses", parametersForLog, "500", ex.Message, ex.StackTrace);
                }
            }

            if (seatmaps.Count == 0)
            {
                seatmaps = null;
            }

            return objLi.BuildAPIResultXML(null, null, null, null, null, seatmaps, null, errors);
        }

        [WebMethod(EnableSession = true, Description = "Get seat map information for transit flight")]
        public APIResult GetSeatMapLayout(string flightId, string originRcd, string destinationRcd, string boardingClass, string bookingClass)
        {
            Mappings objMappings = new Mappings();
            objMappings = (Mappings)Session["Mappings"];

            Library objLi = new Library();
            DataSet ds = new DataSet();
            APIErrors errors = new APIErrors();
            APISeatMaps seatmaps = new APISeatMaps();
            bool isParameterWellForm = true;
            string parametersForLog = string.Empty;
            string codeMessage = "725"; // No seat map information found

            if (string.IsNullOrEmpty(flightId))
            {
                isParameterWellForm = false;
                GetAPIErrors("720", errors);
                LogHelper.writeToLogFile("GetSeatMapLayout", parametersForLog, "720", "Invalid flightId", string.Empty);
            }
            else if (string.IsNullOrEmpty(originRcd))
            {
                isParameterWellForm = false;
                GetAPIErrors("721", errors);
                LogHelper.writeToLogFile("GetSeatMapLayout", parametersForLog, "721", "Invalid originRcd", string.Empty);
            }
            else if (string.IsNullOrEmpty(destinationRcd))
            {
                isParameterWellForm = false;
                GetAPIErrors("722", errors);
                LogHelper.writeToLogFile("GetSeatMapLayout", parametersForLog, "722", "Invalid destinationRcd", string.Empty);
            }
            else if (string.IsNullOrEmpty(boardingClass))
            {
                isParameterWellForm = false;
                GetAPIErrors("723", errors);
                LogHelper.writeToLogFile("GetSeatMapLayout", parametersForLog, "723", "Invalid boardingClass", string.Empty);
            }

            string configuration = string.Empty;

            if (isParameterWellForm)
            {
                try
                {
                    if (objMappings != null && objMappings.Count > 0)
                    {
                        ServiceClient objClient = new ServiceClient();
                        objClient.objService = (TikAeroXMLwebservice)Session["AgentService"];

                        ds = objClient.GetSeatMapLayout(flightId.ToUpper(), originRcd, destinationRcd, boardingClass, configuration, tikAEROWebCheckinAPI.Classes.Language.CurrentCode());

                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            ds.DataSetName = "SeatMapLayout";
                            GetAPISeatMaps(ds.Tables[0].Rows[0]["flight_id"].ToString(), ds.GetXml(), seatmaps);
                            codeMessage = "000";
                        }
                        else
                        {
                            codeMessage = "725";
                        }

                        GetAPIErrors(codeMessage, errors);

                        if (codeMessage != "000")
                        {
                            LogHelper.writeToLogFile("SeatMapLayout", parametersForLog, "725", "No seat map information found", string.Empty);
                        }
                    }
                    else
                    {
                        GetAPIErrors("701", errors);
                        LogHelper.writeToLogFile("SeatMapLayout", parametersForLog, "701", "objMappings is null", string.Empty);
                    }
                }
                catch (Exception ex)
                {
                    GetAPIErrors("700", errors);
                    LogHelper.writeToLogFile("SeatMapLayout", parametersForLog, "700", ex.Message, ex.StackTrace);
                }
            }

            if (seatmaps.Count == 0)
            {
                seatmaps = null;
            }

            return objLi.BuildAPIResultXML(null, null, null, null, null, seatmaps, null, errors);
        }

        [WebMethod(EnableSession = true, Description = "Manually seat select")]
        public APIResult SelectSeat(SeatRequests seat_request)
        {
            Mappings objSaveMapping = new Mappings();

            Mappings objMappings = new Mappings();
            objMappings = (Mappings)Session["Mappings"];

            string strCheckInFlightSession = string.Empty;
            string strGetPassengerDetailXML = string.Empty;
            
            APIErrors errors = new APIErrors();
            APIMessageResults message_results = new APIMessageResults();
            Library objLi = new Library();
            
            bool isParameterWellForm = true;
            string parametersForLog = string.Empty;
            bool isCalculateSeatFee = true;
            bool bSuccess = true;
            bool isIncludeOFFLOADED = false;
            bool isAllowed = false;
            bool isIncludeSeatAssigned = false;
            bool isContainAdult = false;
            int iAdult = 0;
            int iChild = 0;
            int iInfant = 0;
            bool isSeatMatchInfant = false;
            bool isFreeSeating = false;
            bool isDuplicateSeat = false;

            if (seat_request == null || seat_request.Count == 0)
            {
                isParameterWellForm = false;
                GetAPIErrors("810", errors);
                LogHelper.writeToLogFile("SelectSeat", parametersForLog, "810", "SeatRequest is empty", string.Empty);
            }

            if (isParameterWellForm)
            {
                try
                {
                    if (objMappings != null && objMappings.Count > 0)//802
                    {
                        //if (!string.IsNullOrEmpty(strCheckInFlightSession))//801
                        //if (Session["CheckInFlight"] != null && Session["CheckInFlight"].ToString().Length > 0)
                        //{
                            //05-03
                            //check passenger type & OFFLOADED for seat assign from web config
                            string strPassengerTypeBlock = string.Empty;
                            bool isSeatOFFLOADEDAllowed = false;
                            bool isAllowCHDCheckinAlone = false;

                            if (System.Configuration.ConfigurationManager.AppSettings["SeatPassengerTypeBlock"] != null)
                            {
                                strPassengerTypeBlock = System.Configuration.ConfigurationManager.AppSettings["SeatPassengerTypeBlock"].ToUpper();
                            }
                            if (System.Configuration.ConfigurationManager.AppSettings["SeatOFFLOADEDAllowed"] != null)
                            {
                                isSeatOFFLOADEDAllowed = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["SeatOFFLOADEDAllowed"]);
                            }
                            if (System.Configuration.ConfigurationManager.AppSettings["isAllowCHDCheckinAlone"] != null)
                            {
                                isAllowCHDCheckinAlone = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["isAllowCHDCheckinAlone"]);
                            }

                            if (seat_request != null && seat_request.Count > 0)
                            {
                                for (int i = 0; i < seat_request.Count; i++)
                                {
                                    if (seat_request[i].BookingSegmentId.Equals(Guid.Empty))
                                    {
                                        GetAPIErrors("811", errors);
                                        LogHelper.writeToLogFile("SelectSeat", parametersForLog, "811", "One of the request supply an invalid BookingSegmentId", string.Empty);
                                        bSuccess = false;
                                        break;
                                    }
                                    else if (seat_request[i].PassengerId.Equals(Guid.Empty))
                                    {
                                        GetAPIErrors("812", errors);
                                        LogHelper.writeToLogFile("SelectSeat", parametersForLog, "812", "One of the request supply an invalid PassengerId", string.Empty);
                                        bSuccess = false;
                                        break;
                                    }

                                    //if (string.IsNullOrEmpty(seat_request[i].SeatFeeRcd))
                                    //{
                                    //    isCalculateSeatFee = false;
                                    //}
                                }
                            }

                            //When all validation pass do the seat assignment.
                            if (bSuccess == true)
                            {
                                //sort request by passenger_type_rcd
                                string adultPassengerId = string.Empty;
                                string childPassengerId = string.Empty;
                                string infantPassengerId = string.Empty;
                                SeatRequests seats_sort = new SeatRequests();
                                foreach (Mapping m in objMappings)
                                {
                                    switch (m.passenger_type_rcd.ToUpper())
                                    {
                                          //case "ADULT" :
                                          //adultPassengerId += m.passenger_id + "|";
                                          //  break;
                                        case "CHD" :
                                            childPassengerId += m.passenger_id + "|";
                                            break;
                                        case "INF" :
                                            infantPassengerId += m.passenger_id + "|";
                                            break;
                                        default:
                                            adultPassengerId += m.passenger_id + "|";
                                            break;
                                    }
                                }

                                string[] adultIds = adultPassengerId.Split('|');
                                string[] childIds = childPassengerId.Split('|');
                                string[] infantIds = infantPassengerId.Split('|');

                                if (adultIds.Length > 0)
                                {
                                    for (int a = 0; a < adultIds.Length; a++)
                                    {
                                        if (!string.IsNullOrEmpty(adultIds[a]))
                                        {
                                            for (int aa = 0; aa < seat_request.Count; aa++)
                                            {
                                                if (seat_request[aa].PassengerId.ToString() == adultIds[a])
                                                {
                                                    iAdult++;
                                                    isContainAdult = true;
                                                    isDuplicateSeat = false;
                                                    seats_sort.Add(seat_request[aa]);
                                                }
                                            }
                                        }
                                    }

                                    for (int b = 0; b < childIds.Length; b++)
                                    {
                                        if (!string.IsNullOrEmpty(childIds[b]))
                                        {
                                            for (int bb = 0; bb < seat_request.Count; bb++)
                                            {
                                                if (seat_request[bb].PassengerId.ToString() == childIds[b])
                                                {
                                                    iChild++;
                                                    seats_sort.Add(seat_request[bb]);
                                                }
                                            }
                                        }
                                    }

                                    for (int c = 0; c < infantIds.Length; c++)
                                    {
                                        if (!string.IsNullOrEmpty(infantIds[c]))
                                        {
                                            for (int cc = 0; cc < seat_request.Count; cc++)
                                            {
                                                if (seat_request[cc].PassengerId.ToString() == infantIds[c])
                                                {
                                                    iInfant++;
                                                    seats_sort.Add(seat_request[cc]);
                                                }
                                            }
                                        }
                                    }
                                }


                                #region Fill Seat Information to Mapping
                                //Fill Seat Information.
                                for (int i = 0; i < seats_sort.Count; i++)
                                {
                                    for (int j = 0; j < objMappings.Count; j++)
                                    {
                                        if (seats_sort[i].PassengerId.Equals(objMappings[j].passenger_id) &&
                                            seats_sort[i].BookingSegmentId.Equals(objMappings[j].booking_segment_id)
                                            && !strPassengerTypeBlock.Contains(objMappings[j].passenger_type_rcd.ToUpper()) && bSuccess)
                                        {
                                            if (objMappings[j].e_ticket_flag == 1 && objMappings[j].ticket_number.Length > 0)
                                            {
                                                if (objMappings[j].passenger_check_in_status_rcd == null || objMappings[j].passenger_check_in_status_rcd.Length == 0)
                                                {
                                                    isAllowed = true;
                                                    isSeatMatchInfant = false;
                                                }

                                                //05-03
                                                if (objMappings[j].passenger_check_in_status_rcd == "OFFLOADED")
                                                {
                                                    if (!isSeatOFFLOADEDAllowed)
                                                    {
                                                        isIncludeOFFLOADED = true;
                                                    }
                                                    else
                                                    {
                                                        isAllowed = true;
                                                    }
                                                }

                                                //yo 10-02
                                                if (!string.IsNullOrEmpty(objMappings[j].seat_number))
                                                {
                                                    isIncludeSeatAssigned = true;
                                                }

                                                if (objMappings[j].free_seating_flag == 1)
                                                {
                                                    isFreeSeating = true;
                                                }
                                            }

                                            if (isIncludeOFFLOADED)//!isAllowed
                                            {
                                                GetAPIErrors("805", errors); //OFFLOADED passenger can't checkin
                                                LogHelper.writeToLogFile("SelectSeat", parametersForLog, "805", "OFFLOADED passenger can't checkin", string.Empty);
                                                bSuccess = false;
                                            }
                                            else if (isFreeSeating)
                                            {
                                                GetAPIErrors("807", errors); //This flight is free seating
                                                LogHelper.writeToLogFile("SelectSeat", parametersForLog, "807", "This flight is free seating", string.Empty);
                                                bSuccess = false;
                                            }
                                            else if (isIncludeSeatAssigned)
                                            {
                                                GetAPIErrors("806", errors); //Passengers already have been assigned seat
                                                LogHelper.writeToLogFile("SelectSeat", parametersForLog, "806", "Passengers already have been assigned seat", string.Empty);
                                                bSuccess = false;
                                            }
                                            else if (!isContainAdult && !isAllowCHDCheckinAlone)
                                            {
                                                GetAPIErrors("803", errors); //Child or infant can't assign seat independently
                                                LogHelper.writeToLogFile("SelectSeat", parametersForLog, "803", "Child or infant can't assign seat independently", string.Empty);
                                                bSuccess = false;
                                            }
                                            else if (iInfant > iAdult)
                                            {
                                                GetAPIErrors("804", errors); //Number of adults need to be higher than the number of infants
                                                LogHelper.writeToLogFile("SelectSeat", parametersForLog, "804", "Number of adults need to be higher than the number of infants", string.Empty);
                                                bSuccess = false;
                                            }
                                            else if (objMappings[j].passenger_type_rcd.ToUpper() == "INF")
                                            {
                                                //check infant seat match adult seat
                                                foreach (Mapping m in objMappings)
                                                {
                                                    string reqSeatNumber = seats_sort[j].SeatRow + seats_sort[j].SeatColumn;
                                                    if (m.passenger_type_rcd.ToUpper() == "ADULT" && m.seat_number == reqSeatNumber)
                                                    {
                                                        //found seat for infant
                                                        isSeatMatchInfant = true;
                                                    }
                                                }

                                                if (!isSeatMatchInfant)
                                                {
                                                    GetAPIErrors("801", errors); //Infant's seat does not match adult's seat
                                                    LogHelper.writeToLogFile("SelectSeat", parametersForLog, "801", "Infant's seat is not match with adult's seat", string.Empty);
                                                    bSuccess = false;
                                                }
                                                else
                                                {
                                                    //start select seat
                                                    if (objSaveMapping.Count > 0)
                                                    {
                                                        foreach (Mapping map in objSaveMapping)
                                                        {
                                                            if ((map.seat_number == seats_sort[i].SeatRow.ToString() + seats_sort[i].SeatColumn) && (map.passenger_type_rcd.ToUpper() == "INF"))
                                                            {
                                                                isDuplicateSeat = true;
                                                            }
                                                        }
                                                    }

                                                    if (!isDuplicateSeat)
                                                    {
                                                        objMappings[j].seat_column = seats_sort[i].SeatColumn;
                                                        objMappings[j].seat_row = seats_sort[i].SeatRow;
                                                        if (seat_request[i].SeatRow == 0 && string.IsNullOrEmpty(seats_sort[i].SeatColumn))
                                                        {
                                                            objMappings[j].seat_number = string.Empty;
                                                        }
                                                        else
                                                        {
                                                            objMappings[j].seat_number = seats_sort[i].SeatRow.ToString() + seats_sort[i].SeatColumn;
                                                        }
                                                        objMappings[j].seat_fee_rcd = seats_sort[i].SeatFeeRcd;

                                                        objSaveMapping.Add(objMappings[j]);
                                                    }
                                                    else
                                                    {
                                                        //duplicate seat
                                                        GetAPIErrors("809", errors); //Duplicate seat selected
                                                        LogHelper.writeToLogFile("SelectSeat", parametersForLog, "809", "Duplicate seat selected", string.Empty);
                                                        bSuccess = false;
                                                    }
 
                                                }
                                            }
                                            else
                                            {
                                                //start select seat
                                                if (objSaveMapping.Count > 0)
                                                {
                                                    foreach (Mapping map in objSaveMapping)
                                                    {
                                                        if (map.seat_number == seats_sort[i].SeatRow.ToString() + seats_sort[i].SeatColumn)
                                                        {
                                                            isDuplicateSeat = true;
                                                        }
                                                    }
                                                }

                                                if (!isDuplicateSeat)
                                                {
                                                    objMappings[j].seat_column = seats_sort[i].SeatColumn;
                                                    objMappings[j].seat_row = seats_sort[i].SeatRow;
                                                    if (seat_request[i].SeatRow == 0 && string.IsNullOrEmpty(seats_sort[i].SeatColumn))
                                                    {
                                                        objMappings[j].seat_number = string.Empty;
                                                    }
                                                    else
                                                    {
                                                        objMappings[j].seat_number = seats_sort[i].SeatRow.ToString() + seats_sort[i].SeatColumn;
                                                    }
                                                    objMappings[j].seat_fee_rcd = seats_sort[i].SeatFeeRcd;

                                                    objSaveMapping.Add(objMappings[j]);
                                                }
                                                else
                                                {
                                                    //duplicate seat
                                                    GetAPIErrors("809", errors); //Duplicate seat selected
                                                    LogHelper.writeToLogFile("SelectSeat", parametersForLog, "809", "Duplicate seat selected", string.Empty);
                                                    bSuccess = false;
                                                }
                                            }
                                        }
                                        else { }
                                        
                                    }
                                }
                                #endregion

                                if (bSuccess)
                                {
                                    if (objSaveMapping.Count == 0)
                                    {
                                        GetAPIErrors("808", errors); //Passengers do not match booking_segment_id
                                        LogHelper.writeToLogFile("SelectSeat", parametersForLog, "807", "Passengers do not match booking_segment_id", string.Empty);
                                        bSuccess = false;
                                    }
                                    else
                                    {
                                        Session["SaveMapping"] = objSaveMapping;
                                    }
                                }
                            }

                    }
                    else
                    {
                        GetAPIErrors("802", errors);
                        LogHelper.writeToLogFile("SelectSeat", parametersForLog, "802", "Session[Mappings] is null", string.Empty);
                        bSuccess = false;
                    }

                    //If failed write to log file.
                    if (bSuccess == true)
                    {
                        APIMessageResult message_result = new APIMessageResult();
                        message_result.message_result = "Process complete";
                        message_results.Add(message_result);
                        GetAPIErrors("000", errors);
                    }


                }
                catch (Exception ex)
                {
                    GetAPIErrors("800", errors);
                    LogHelper.writeToLogFile("SelectSeat", parametersForLog, "800", ex.Message, ex.StackTrace);
                    bSuccess = false;
                }

            }

            if (message_results.Count == 0)
            {
                message_results = null;
            }

            return objLi.BuildAPIResultXML(null, null, null, null, null, null, message_results, errors);
        }

        #region Test Function
        //[WebMethod(EnableSession = true, Description = "Test assignSeats method with defined parameter")]
        //public APIResult TestAssignSeats()
        //{
        //    string[] strPassengerIds = { 
        //                                  "9383b09d-d912-47a0-acd4-76eb6f93de94",
        //                                  "c1d4c85f-8983-421e-b9fa-5fdf910ec1ae"
        //                              };
        //    return AssignSeats("d0dc8852-4292-4616-8614-21d7422d9b68", strPassengerIds);
        //}
        //[WebMethod(EnableSession = true, Description = "Test mommit method with defined parameter")]
        //public APIResult TestCommit()
        //{
        //    string[] strPassengerIds = { 
        //                                  "298ebb19-b1fe-4e5e-b855-5ec7ca1ccccc",
        //                                  "71d28155-9c33-46a0-b92e-25638d6ecba4",
        //                                  "9e2e9e58-2119-4a3b-b6d5-4f0b4879bb46",
        //                                  "8562b96c-54cc-4bbd-9e22-c9d96c482372",
        //                                  "77695e5c-8003-410e-96fe-882b8b8893f5"
        //                              };
        //    return Commit("3e38f960-da8b-462b-85a3-c17f84923256", strPassengerIds);
        //}
        //[WebMethod(EnableSession = true, Description = "Test getBoardingPassess method with defined parameter")]
        //public APIResult TestGetBoardingPassess()
        //{
        //    string[] strPassengerIds = { 
        //                                  "298ebb19-b1fe-4e5e-b855-5ec7ca1ccccc",
        //                                  "71d28155-9c33-46a0-b92e-25638d6ecba4",
        //                                  "9e2e9e58-2119-4a3b-b6d5-4f0b4879bb46",
        //                                  "8562b96c-54cc-4bbd-9e22-c9d96c482372",
        //                                  "77695e5c-8003-410e-96fe-882b8b8893f5"
        //                              };
        //    return GetBoardingPasses("3e38f960-da8b-462b-85a3-c17f84923256", strPassengerIds);
        //}
        //[WebMethod(EnableSession = true, Description = "Test get seatmap")]
        //public APIResult TestGetSeatmap()
        //{
        //    string originRCD = "ACH";
        //    string destinationRCD = "FDH";
        //    Guid flightId = new Guid("640AF451-B519-4DA9-8317-676C712A2255");
        //    string boardingClass = "Y";
        //    string bookingClass = "Y";
        //    return GetSeatMap(originRCD, destinationRCD, flightId, boardingClass, bookingClass);
        //}

        //[WebMethod(EnableSession = true, Description = "Test UpdatePassengerDocumentDetails")]
        //public APIResult TestUpdatePassengerDocumentDetails()
        //{
        //    APIPassengerUpdateRequests requests = new APIPassengerUpdateRequests();
        //    APIPassengerUpdateRequest req = new APIPassengerUpdateRequest();
        //    req.date_of_birth = DateTime.Now;
        //    req.document_type_rcd = "PP";
        //    req.gender_type_rcd = "M";
        //    req.nationality_rcd = "TH";
        //    req.passenger_id = new Guid("c0d4487a-097a-4d97-8304-d6197e87763c");
        //    req.passport_expiry_date = DateTime.Now;
        //    req.passport_issue_country_rcd = "THA";
        //    req.passport_number = "234567";

        //    APIPassengerUpdateRequest req2 = new APIPassengerUpdateRequest();
        //    req2.date_of_birth = DateTime.Now;
        //    req2.document_type_rcd = "PP";
        //    req2.gender_type_rcd = "F";
        //    req2.nationality_rcd = "TH";
        //    req2.passenger_id = new Guid("1075a327-f7d7-41e2-9bd9-14233da84155");
        //    req2.passport_expiry_date = DateTime.Now;
        //    req2.passport_issue_country_rcd = "THA";
        //    req2.passport_number = "123456";

        //    APIPassengerUpdateRequest req3 = new APIPassengerUpdateRequest();
        //    req3.date_of_birth = DateTime.Now;
        //    req3.document_type_rcd = "PP";
        //    req3.gender_type_rcd = "M";
        //    req3.nationality_rcd = "TH";
        //    req3.passenger_id = new Guid("8db7a694-dc2d-4209-8a31-6496c4fcde4a");
        //    req3.passport_expiry_date = DateTime.Now;
        //    req3.passport_issue_country_rcd = "THA";
        //    req3.passport_number = "345678";

        //    requests.Add(req);
        //    requests.Add(req2);
        //    requests.Add(req3);

        //    return UpdatePassengerDocumentDetails(requests);
        //}

        //[WebMethod(EnableSession = true, Description = "Test select seat")]
        //public APIResult TestSelectSeat()
        //{
        //    SeatRequests list = new SeatRequests();

        //    SeatRequest req = new SeatRequest();
        //    req.BookingSegmentId = new Guid("34ce660a-1869-4faf-884f-2b4aceff8868");
        //    req.PassengerId = new Guid("de020f51-4a06-40a9-8dae-88e9b90580cc");
        //    req.SeatColumn = "A";
        //    req.SeatRow = 2;
        //    req.SeatFeeRcd = "";

        //    SeatRequest req2 = new SeatRequest();
        //    req2.BookingSegmentId = new Guid("34ce660a-1869-4faf-884f-2b4aceff8868");
        //    req2.PassengerId = new Guid("c93dee93-277d-45a4-a5bd-4940e105b9ae");
        //    req2.SeatColumn = "B";
        //    req2.SeatRow = 2;
        //    req2.SeatFeeRcd = "";

        //    SeatRequest req3 = new SeatRequest();
        //    req3.BookingSegmentId = new Guid("34ce660a-1869-4faf-884f-2b4aceff8868");
        //    req3.PassengerId = new Guid("959c811d-b825-4ef1-9c8d-59d3559247f6");
        //    req3.SeatColumn = "C";
        //    req3.SeatRow = 2;
        //    req3.SeatFeeRcd = "";

        //    SeatRequest req4 = new SeatRequest();
        //    req4.BookingSegmentId = new Guid("34ce660a-1869-4faf-884f-2b4aceff8868");
        //    req4.PassengerId = new Guid("f99319f9-2c35-4798-b1c3-ca8d7813e47c");
        //    req4.SeatColumn = "D";
        //    req4.SeatRow = 2;
        //    req4.SeatFeeRcd = "";

        //    SeatRequest req5 = new SeatRequest();
        //    req5.BookingSegmentId = new Guid("34ce660a-1869-4faf-884f-2b4aceff8868");
        //    req5.PassengerId = new Guid("de79b86e-2647-4cb0-863e-f01c4f74e054");
        //    req5.SeatColumn = "E";
        //    req5.SeatRow = 2;
        //    req5.SeatFeeRcd = "";

        //    SeatRequest req6 = new SeatRequest();
        //    req6.BookingSegmentId = new Guid("34ce660a-1869-4faf-884f-2b4aceff8868");
        //    req6.PassengerId = new Guid("196e258e-0683-42f5-a5da-c34f85df25d6");
        //    req6.SeatColumn = "F";
        //    req6.SeatRow = 2;
        //    req6.SeatFeeRcd = "";

        //    SeatRequest req7 = new SeatRequest();
        //    req7.BookingSegmentId = new Guid("34ce660a-1869-4faf-884f-2b4aceff8868");
        //    req7.PassengerId = new Guid("bd42e9ca-b4f4-40ec-a64d-09909de6c807");
        //    req7.SeatColumn = "A";
        //    req7.SeatRow = 2;
        //    req7.SeatFeeRcd = "";

        //    SeatRequest req8 = new SeatRequest();
        //    req8.BookingSegmentId = new Guid("34ce660a-1869-4faf-884f-2b4aceff8868");
        //    req8.PassengerId = new Guid("ccaa0423-7e37-4d59-824c-761796925b51");
        //    req8.SeatColumn = "B";
        //    req8.SeatRow = 2;
        //    req8.SeatFeeRcd = "";

        //    SeatRequest req9 = new SeatRequest();
        //    req9.BookingSegmentId = new Guid("34ce660a-1869-4faf-884f-2b4aceff8868");
        //    req9.PassengerId = new Guid("9c4f3cbe-0740-4490-a24f-905b6a13d461");
        //    req9.SeatColumn = "C";
        //    req9.SeatRow = 2;
        //    req9.SeatFeeRcd = "";

        //    list.Add(req7);
        //    list.Add(req8);
        //    list.Add(req9);
        //    list.Add(req4);
        //    list.Add(req5);
        //    list.Add(req6);
        //    list.Add(req);
        //    list.Add(req2);
        //    list.Add(req3);


        //    //SeatRequest req = new SeatRequest();
        //    //req.BookingSegmentId = new Guid("d6d8d042-0314-4129-a85b-1de385502363");
        //    //req.PassengerId = new Guid("e5f1e651-3910-4eb0-a83e-b72683578376");
        //    //req.SeatColumn = "A";
        //    //req.SeatRow = 1;
        //    //req.SeatFeeRcd = "";

        //    //SeatRequest req2 = new SeatRequest();
        //    //req2.BookingSegmentId = new Guid("d6d8d042-0314-4129-a85b-1de385502363");
        //    //req2.PassengerId = new Guid("33c9e2f2-174a-448e-b0e8-2a562ac2baea");
        //    //req2.SeatColumn = "A";
        //    //req2.SeatRow = 1;
        //    //req2.SeatFeeRcd = "";

        //    //SeatRequest req3 = new SeatRequest();
        //    //req3.BookingSegmentId = new Guid("d6d8d042-0314-4129-a85b-1de385502363");
        //    //req3.PassengerId = new Guid("3338fd3c-f9c7-4731-a325-8a78c40b18eb");
        //    //req3.SeatColumn = "A";
        //    //req3.SeatRow = 1;
        //    //req3.SeatFeeRcd = "";

        //    //list.Add(req);
        //    //list.Add(req2);
        //    //list.Add(req3);

        //    return SelectSeat(list);
        //}
        #endregion
        #region Helper
        private void InitializeService()
        {

            HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            
            Booking objBooking = new Booking();
            objBooking = (Booking)Session["Booking"];

            Library objLi = new Library();

            if (objBooking == null)
            {
                objBooking = new Booking();
            }

            if (Session["CheckInFlight"] != null)
            {
                Session.Remove("CheckInFlight");
            }
            if (Session["Mappings"] != null)
            {
                Session.Remove("Mappings");
            }
            if (Session["Itinerary"] != null)
            {
                Session.Remove("Itinerary");
            }
            if (Session["Passengers"] != null)
            {
                Session.Remove("Passengers");
            }

            if (Session.IsNewSession == true || Session["Booking"] == null)
            {
                Agents objAgent = new Agents();
                WebCheckinAPIVariable objUv = new WebCheckinAPIVariable();

                if (ConfigurationManager.AppSettings["Service"] == "1")
                {
                    //Initialize Agentservice
                    if (Session["AgentService"] == null)
                    {
                        ServiceClient objService = new ServiceClient();
                        objUv.UserId = new Guid(ConfigurationManager.AppSettings["UserId"]);
                        objService.initializeWebService(ConfigurationManager.AppSettings["DefaultAgencyCode"], ref objAgent);
                        Session["AgentService"] = objService.objService;
                        objService = null;
                    }
                }
                else
                {
                    //Get B2c agency information
                    if (objAgent == null || objAgent.Count == 0)
                    {
                        //objAgent.GetAgencyInformation(ConfigurationManager.AppSettings["DefaultAgencyCode"]);
                        //objBooking.UserId = new Guid(ConfigurationManager.AppSettings["UserId"]);
                        //objAgent.AgencyRead(ConfigurationManager.AppSettings["DefaultAgencyCode"], string.Empty);
                        //objUv.UserId = new Guid(ConfigurationManager.AppSettings["UserId"]);
                    }
                }

                Session["Booking"] = objBooking;
            }

        }
        private string SingleFlight(XPathNavigator nv)
        {
            string tempSegmentId = string.Empty;
            string segmentId = string.Empty;

            XPathExpression xe = nv.Compile("Booking/Mapping");
            Library objLi = new Library();

            xe.AddSort("booking_segment_id", XmlSortOrder.Ascending, XmlCaseOrder.None, string.Empty, XmlDataType.Text);
            foreach (XPathNavigator n in nv.Select(xe))
            {
                segmentId = objLi.getXPathNodevalue(n, "booking_segment_id", Library.xmlReturnType.value);
                if (tempSegmentId.Length == 0)
                { tempSegmentId = segmentId; }

                if (tempSegmentId != segmentId)
                { return string.Empty; }
            }

            return tempSegmentId;
        }
        private string GetFlightInformation(string strBookingSegmentId)
        {
            Library objLi = new Library();
            Helper objHelper = new Helper();

            Mappings mps = new Mappings();
            Itinerary it = new Itinerary();
            Passengers paxs = new Passengers();

            CheckInPassengers ckps = new CheckInPassengers();

            objHelper.GetFlightInformation(ref ckps, strBookingSegmentId, Session["CheckInFlight"].ToString());

            ckps.objService = (TikAeroXMLwebservice)Session["AgentService"];

            string strXml = ckps.GetPassengerDetails("EN");
            if (strBookingSegmentId.Length == 0)
            {
                objLi.AddItinerary(strXml, it);
                objLi.AddMappings(strXml, mps);
            }
            else
            {
                objLi.AddItinerary(strXml, it, strBookingSegmentId);
                objLi.AddMappings(strXml, mps, strBookingSegmentId);
            }
            objLi.AddPassengers(strXml, paxs);

            Session["Mappings"] = mps;
            Session["Itinerary"] = it;
            Session["Passengers"] = paxs;

            return strXml;

        }
        private decimal GetOutstandingBalance(XPathDocument xmlDoc)
        {
            XPathNavigator nv = xmlDoc.CreateNavigator();
            Library objLi = new Library();
            decimal dAmount = 0;

            foreach (XPathNavigator n in nv.Select("Booking/Mapping"))
            {
                dAmount = (Convert.ToDecimal(objLi.getXPathNodevalue(n, "ticket_total", Library.xmlReturnType.value)) + Convert.ToDecimal(objLi.getXPathNodevalue(n, "fee_total", Library.xmlReturnType.value))) -
                          (Convert.ToDecimal(objLi.getXPathNodevalue(n, "ticket_payment_total", Library.xmlReturnType.value)) + Convert.ToDecimal(objLi.getXPathNodevalue(n, "fee_payment_total", Library.xmlReturnType.value)));
            }

            return dAmount;
        }

        private APIFlightSegments GetAPIFlightSegments(string bookingSegmentId, string strGetPassengerDetailXML, string strCheckInFlightXML, APIFlightSegments fss)
        {
            Library objLi = new Library();
            //APIFlightSegment fs;

            XPathDocument xmlDoc = new XPathDocument(new StringReader(strGetPassengerDetailXML));
            XPathNavigator nv = xmlDoc.CreateNavigator();

            XPathDocument xmlDoc2 = new XPathDocument(new StringReader(strCheckInFlightXML));
            XPathNavigator nv2 = xmlDoc2.CreateNavigator();

            foreach (XPathNavigator n in nv.Select("Booking/FlightSegment[booking_segment_id = '"+ bookingSegmentId +"']"))
            {
                APIFlightSegment fs = new APIFlightSegment();
                fs.booking_segment_id           = XmlHelper.XpathValueNullToGUID(n, "booking_segment_id");
                fs.flight_id                    = XmlHelper.XpathValueNullToGUID(n, "flight_id");
                fs.flight_connection_id         = XmlHelper.XpathValueNullToEmpty(n, "flight_connection_id");
                fs.airline_rcd                  = XmlHelper.XpathValueNullToEmpty(n, "airline_rcd");
                fs.flight_number                = XmlHelper.XpathValueNullToEmpty(n, "flight_number");
                fs.departure_date               = XmlHelper.XpathValueNullToDateTime(n, "departure_date");
                fs.departure_time               = int.Parse(XmlHelper.XpathValueNullToZero(n, "departure_time").ToString());
                fs.booking_class_rcd            = XmlHelper.XpathValueNullToEmpty(n, "booking_class_rcd");
                fs.boarding_class_rcd           = XmlHelper.XpathValueNullToEmpty(n, "boarding_class_rcd");
                fs.segment_status_rcd           = XmlHelper.XpathValueNullToEmpty(n, "segment_status_rcd");
                fs.booking_id                   = XmlHelper.XpathValueNullToGUID(n, "booking_id");
                fs.number_of_units              = int.Parse(XmlHelper.XpathValueNullToZero(n, "number_of_units").ToString());
                fs.journey_time                 = int.Parse(XmlHelper.XpathValueNullToZero(n, "journey_time").ToString());
                fs.arrival_time                 = int.Parse(XmlHelper.XpathValueNullToZero(n, "arrival_time").ToString());
                fs.arrival_date                 = XmlHelper.XpathValueNullToDateTime(n, "arrival_date");
                fs.origin_rcd                   = XmlHelper.XpathValueNullToEmpty(n, "origin_rcd");
                fs.destination_rcd              = XmlHelper.XpathValueNullToEmpty(n, "destination_rcd");
                fs.segment_change_status_rcd    = XmlHelper.XpathValueNullToEmpty(n, "segment_change_status_rcd");
                fs.info_segment_flag            = byte.Parse(XmlHelper.XpathValueNullToZero(n, "info_segment_flag").ToString());
                fs.high_priority_waitlist_flag  = byte.Parse(XmlHelper.XpathValueNullToZero(n, "high_priority_waitlist_flag").ToString());
                fs.od_origin_rcd                = XmlHelper.XpathValueNullToEmpty(n, "od_origin_rcd");
                fs.flight_check_in_status_rcd   = XmlHelper.XpathValueNullToEmpty(n, "flight_check_in_status_rcd");
                fs.od_destination_rcd           = XmlHelper.XpathValueNullToEmpty(n, "od_destination_rcd");
                fs.origin_name                  = XmlHelper.XpathValueNullToEmpty(n, "origin_name");
                fs.destination_name             = XmlHelper.XpathValueNullToEmpty(n, "destination_name");
                fs.segment_status_name          = XmlHelper.XpathValueNullToEmpty(n, "segment_status_name");
                fs.seatmap_flag                 = byte.Parse(XmlHelper.XpathValueNullToZero(n, "seatmap_flag").ToString());
                fs.allow_web_checkin_flag       = byte.Parse(XmlHelper.XpathValueNullToZero(n, "allow_web_checkin_flag").ToString());
                fs.transit_points               = XmlHelper.XpathValueNullToEmpty(n, "transit_points");
                fs.transit_points_name          = XmlHelper.XpathValueNullToEmpty(n, "transit_points_name");

                foreach (XPathNavigator n2 in nv2.Select("Booking/Mapping[booking_segment_id = '" + bookingSegmentId + "'][booking_id = '" + fs.booking_id.ToString() + "']"))
                {
                    fs.require_open_status_flag         = short.Parse(XmlHelper.XpathValueNullToZero(n2, "require_open_status_flag").ToString());
                    fs.require_ticket_number_flag       = short.Parse(XmlHelper.XpathValueNullToZero(n2, "require_ticket_number_flag").ToString());
                    fs.require_passenger_title_flag     = short.Parse(XmlHelper.XpathValueNullToZero(n2, "require_passenger_title_flag").ToString());
                    fs.require_passenger_gender_flag    = short.Parse(XmlHelper.XpathValueNullToZero(n2, "require_passenger_gender_flag").ToString());
                    fs.require_date_of_birth_flag       = short.Parse(XmlHelper.XpathValueNullToZero(n2, "require_date_of_birth_flag").ToString());
                    fs.require_document_details_flag    = short.Parse(XmlHelper.XpathValueNullToZero(n2, "require_document_details_flag").ToString());
                    fs.require_passenger_weight_flag    = short.Parse(XmlHelper.XpathValueNullToZero(n2, "require_passenger_weight_flag").ToString());
                    fs.show_redress_number_flag         = short.Parse(XmlHelper.XpathValueNullToZero(n2, "show_redress_number_flag").ToString());

                    fs.nautical_miles                   = int.Parse(XmlHelper.XpathValueNullToZero(n2, "nautical_miles").ToString());
                    fs.flight_information_1             = XmlHelper.XpathValueNullToEmpty(n2, "flight_information_1");
                    fs.flight_information_2             = XmlHelper.XpathValueNullToEmpty(n2, "flight_information_2");
                    fs.flight_information_3             = XmlHelper.XpathValueNullToEmpty(n2, "flight_information_3");
                    fs.min_transit_minutes              = int.Parse(XmlHelper.XpathValueNullToZero(n2, "min_transit_minutes").ToString());
                    fs.max_transit_minutes              = int.Parse(XmlHelper.XpathValueNullToZero(n2, "max_transit_minutes").ToString());
                    fs.close_web_check_in               = XmlHelper.XpathValueNullToEmpty(n2, "close_web_check_in");
                    fs.paper_ticket_warning_flag        = short.Parse(XmlHelper.XpathValueNullToZero(n2, "paper_ticket_warning_flag").ToString());

                    string passenger_id = objLi.getXPathNodevalue(n2, "passenger_id", Library.xmlReturnType.value);
                    foreach (XPathNavigator n3 in nv.Select("Booking/Mapping[booking_segment_id = '" + bookingSegmentId + "'][passenger_id = '" + passenger_id + "']"))
                    {
                        fs.currency_rcd                 = XmlHelper.XpathValueNullToEmpty(n3, "currency_rcd");
                        fs.excess_baggage_charge_amount = decimal.Parse(XmlHelper.XpathValueNullToZero(n3, "excess_baggage_charge_amount").ToString());
                    }
                
                }

                fss.Add(fs);
            }
            return fss;
        }
        private APIPassengerMappings GetAPIPassengerMappings(string bookingSegmentId, string passengerId, string strGetPassengerDetailXML, APIPassengerMappings mappings)
        {
            Library objLi = new Library();
            //APIPassengerMapping mapping;

            Mappings mm = new Mappings();
            mm = (Mappings)Session["SaveMapping"];

            XPathDocument xmlDoc = new XPathDocument(new StringReader(strGetPassengerDetailXML));
            XPathNavigator nv = xmlDoc.CreateNavigator();

            foreach (XPathNavigator n in nv.Select("Booking/Mapping[booking_segment_id = '" + bookingSegmentId + "']"))
            {
                APIPassengerMapping mapping = new APIPassengerMapping();
                mapping.passenger_id        = XmlHelper.XpathValueNullToGUID(n, "passenger_id");
                mapping.booking_segment_id  = XmlHelper.XpathValueNullToGUID(n, "booking_segment_id");

                foreach (XPathNavigator n2 in nv.Select("Booking/Mapping[booking_segment_id = '" + bookingSegmentId + "'][passenger_id = '"+ mapping.passenger_id.ToString() +"']"))
                {
                    mapping.booking_id                      = XmlHelper.XpathValueNullToGUID(n, "booking_id");
                    mapping.record_locator                  = XmlHelper.XpathValueNullToEmpty(n2, "record_locator_display");
                    mapping.language_rcd                    = XmlHelper.XpathValueNullToEmpty(n2, "language_rcd");
                    mapping.agency_code                     = XmlHelper.XpathValueNullToEmpty(n2, "agency_code");

                    mapping.seat_number = XmlHelper.XpathValueNullToEmpty(n2, "seat_number");
                    mapping.seat_row = int.Parse(XmlHelper.XpathValueNullToZero(n2, "seat_row").ToString());
                    mapping.seat_column = XmlHelper.XpathValueNullToEmpty(n2, "seat_column");

                    mapping.passenger_check_in_status_rcd = XmlHelper.XpathValueNullToEmpty(n2, "passenger_check_in_status_rcd");
                    mapping.passenger_status_rcd = XmlHelper.XpathValueNullToEmpty(n2, "passenger_status_rcd");
                    mapping.standby_flag = byte.Parse(XmlHelper.XpathValueNullToZero(n, "standby_flag").ToString());

                    if (mm != null)
                    {
                        foreach (Mapping m in mm)
                        {
                            if (m.passenger_id == mapping.passenger_id)
                            {
                                //assign seat
                                mapping.seat_number                     = m.seat_number;
                                mapping.seat_row                        = m.seat_row;
                                mapping.seat_column                     = m.seat_column;

                                //commit
                                mapping.passenger_check_in_status_rcd   = m.passenger_check_in_status_rcd;
                                mapping.passenger_status_rcd            = m.passenger_status_rcd;
                                mapping.standby_flag                    = m.standby_flag;
                            }
                        }
                    }
                    
                    mapping.baggage_weight                  = decimal.Parse(XmlHelper.XpathValueNullToZero(n2, "baggage_weight").ToString());
                    mapping.piece_allowance                 = int.Parse(XmlHelper.XpathValueNullToZero(n2, "piece_allowance").ToString());
                    mapping.airline_rcd                     = XmlHelper.XpathValueNullToEmpty(n2, "airline_rcd");
                    mapping.flight_number                   = XmlHelper.XpathValueNullToEmpty(n2, "flight_number");
                    mapping.boarding_class_rcd              = XmlHelper.XpathValueNullToEmpty(n2, "boarding_class_rcd");
                    mapping.departure_date                  = XmlHelper.XpathValueNullToDateTime(n2, "departure_date");
                    mapping.lastname                        = XmlHelper.XpathValueNullToEmpty(n2, "lastname");
                    mapping.firstname                       = XmlHelper.XpathValueNullToEmpty(n2, "firstname");
                    mapping.gender_type_rcd                 = XmlHelper.XpathValueNullToEmpty(n2, "gender_type_rcd");
                    mapping.title_rcd                       = XmlHelper.XpathValueNullToEmpty(n2, "title_rcd");
                    mapping.passenger_type_rcd              = XmlHelper.XpathValueNullToEmpty(n2, "passenger_type_rcd");
                    mapping.boarding_pass_number            = XmlHelper.XpathValueNullToEmpty(n2, "boarding_pass_number");
                    mapping.check_in_sequence               = int.Parse(XmlHelper.XpathValueNullToZero(n2, "check_in_sequence").ToString());
                    mapping.group_sequence                  = int.Parse(XmlHelper.XpathValueNullToZero(n2, "group_sequence").ToString());
                    
                    mapping.e_ticket_flag                   = byte.Parse(XmlHelper.XpathValueNullToZero(n, "e_ticket_flag").ToString());
                    mapping.fare_type_rcd                   = XmlHelper.XpathValueNullToEmpty(n2, "fare_type_rcd");
                    mapping.booking_class_rcd               = XmlHelper.XpathValueNullToEmpty(n2, "booking_class_rcd");
                    mapping.currency_rcd                    = XmlHelper.XpathValueNullToEmpty(n2, "currency_rcd");
                    mapping.origin_rcd                      = XmlHelper.XpathValueNullToEmpty(n2, "origin_rcd");
                    mapping.destination_rcd                 = XmlHelper.XpathValueNullToEmpty(n2, "destination_rcd");
                    
                    mapping.client_number                   = long.Parse(XmlHelper.XpathValueNullToZero(n2, "client_number").ToString());
                    mapping.vip_flag                        = byte.Parse(XmlHelper.XpathValueNullToZero(n, "vip_flag").ToString());
                    mapping.passenger_weight                = XmlHelper.XpathValueNullToZero(n2, "passenger_weight");
                    
                    mapping.ticket_number                   = XmlHelper.XpathValueNullToEmpty(n2, "ticket_number");
                    
                    //mapping
                    mapping.priority_code                   = XmlHelper.XpathValueNullToEmpty(n2, "priority_code");
                    mapping.date_of_birth                   = XmlHelper.XpathValueNullToDateTime(n2, "date_of_birth");
                    mapping.onward_airline_rcd              = XmlHelper.XpathValueNullToEmpty(n2, "onward_airline_rcd");
                    mapping.onward_flight_number            = XmlHelper.XpathValueNullToEmpty(n2, "onward_flight_number");
                    mapping.onward_departure_date           = XmlHelper.XpathValueNullToDateTime(n2, "onward_departure_date");
                    mapping.onward_departure_time           = int.Parse(XmlHelper.XpathValueNullToZero(n2, "onward_departure_time").ToString());
                    mapping.onward_origin_rcd               = XmlHelper.XpathValueNullToEmpty(n2, "onward_origin_rcd");
                    mapping.onward_destination_rcd          = XmlHelper.XpathValueNullToEmpty(n2, "onward_destination_rcd");
                    mapping.onward_booking_class_rcd        = XmlHelper.XpathValueNullToEmpty(n2, "onward_booking_class_rcd");
                    //mapping
                    mapping.group_name                      = XmlHelper.XpathValueNullToEmpty(n2, "group_name");
                    mapping.contact_name                    = XmlHelper.XpathValueNullToEmpty(n2, "contact_name");
                    mapping.contact_email                   = XmlHelper.XpathValueNullToEmpty(n2, "contact_email");
                    mapping.phone_mobile                    = XmlHelper.XpathValueNullToEmpty(n2, "phone_mobile");
                    mapping.phone_home                      = XmlHelper.XpathValueNullToEmpty(n2, "phone_home");
                    mapping.phone_business                  = XmlHelper.XpathValueNullToEmpty(n2, "phone_business");
                    mapping.received_from                   = XmlHelper.XpathValueNullToEmpty(n2, "received_from");
                    mapping.phone_fax                       = XmlHelper.XpathValueNullToEmpty(n2, "phone_fax");
                    mapping.vendor_rcd                      = XmlHelper.XpathValueNullToEmpty(n2, "vendor_rcd");
                    mapping.tour_operator_locator           = XmlHelper.XpathValueNullToEmpty(n2, "tour_operator_locator");

                    mapping.coupon_number                   = XmlHelper.XpathValueNullToEmpty(n2, "coupon_number");
                    mapping.onward_segment_status_rcd       = XmlHelper.XpathValueNullToEmpty(n2, "onward_segment_status_rcd");
                    mapping.previous_airline_rcd            = XmlHelper.XpathValueNullToEmpty(n2, "previous_airline_rcd");
                    mapping.previous_flight_number          = XmlHelper.XpathValueNullToEmpty(n2, "previous_flight_number");
                    mapping.previous_departure_date         = XmlHelper.XpathValueNullToDateTime(n2, "previous_departure_date");
                    mapping.previous_departure_time         = int.Parse(XmlHelper.XpathValueNullToZero(n2, "previous_departure_time").ToString());
                    mapping.previous_origin_rcd             = XmlHelper.XpathValueNullToEmpty(n2, "previous_origin_rcd");
                    mapping.previous_destination_rcd        = XmlHelper.XpathValueNullToEmpty(n2, "previous_destination_rcd");
                    mapping.previous_booking_class_rcd_sort = int.Parse(XmlHelper.XpathValueNullToZero(n2, "previous_booking_class_rcd_sort").ToString());
                    mapping.previous_segment_status_rcd     = XmlHelper.XpathValueNullToEmpty(n2, "previous_segment_status_rcd");
                    mapping.arrival_date                    = XmlHelper.XpathValueNullToDateTime(n2, "arrival_date");
                    mapping.departure_time                  = int.Parse(XmlHelper.XpathValueNullToZero(n2, "departure_time").ToString());
                    mapping.arrival_time                    = int.Parse(XmlHelper.XpathValueNullToZero(n2, "arrival_time").ToString());
                    mapping.group_count                     = int.Parse(XmlHelper.XpathValueNullToZero(n2, "group_count").ToString());
                    mapping.ticket_total                    = XmlHelper.XpathValueNullToZero(n2, "ticket_total");
                    mapping.ticket_payment_total            = XmlHelper.XpathValueNullToZero(n2, "ticket_payment_total");
                    mapping.fee_total                       = XmlHelper.XpathValueNullToZero(n2, "fee_total");
                    mapping.fee_payment_total               = XmlHelper.XpathValueNullToZero(n2, "fee_payment_total");

                    mapping.boarding_gate                   = XmlHelper.XpathValueNullToEmpty(n2, "boarding_gate");
                    mapping.boarding_time                   = int.Parse(XmlHelper.XpathValueNullToZero(n2, "boarding_time").ToString());

                    foreach (XPathNavigator n3 in nv.Select("Booking/Passenger[booking_id = '" + mapping.booking_id.ToString() + "'][passenger_id = '" + mapping.passenger_id.ToString() + "']"))
                    {
                        //passenger
                        mapping.member_number                       = XmlHelper.XpathValueNullToEmpty(n3, "member_number");
                        mapping.member_level_rcd                    = XmlHelper.XpathValueNullToEmpty(n3, "member_level_rcd");
                        mapping.nationality_rcd                     = XmlHelper.XpathValueNullToEmpty(n3, "nationality_rcd");
                        mapping.passport_number                     = XmlHelper.XpathValueNullToEmpty(n3, "passport_number");
                        mapping.passport_issue_date                 = XmlHelper.XpathValueNullToDateTime(n3, "passport_issue_date");
                        mapping.passport_expiry_date                = XmlHelper.XpathValueNullToDateTime(n3, "passport_expiry_date");
                        mapping.passport_issue_place                = XmlHelper.XpathValueNullToEmpty(n3, "passport_issue_place");
                        mapping.passport_birth_place                = XmlHelper.XpathValueNullToEmpty(n3, "passport_birth_place");
                        mapping.wheelchair_flag                     = byte.Parse(XmlHelper.XpathValueNullToZero(n3, "wheelchair_flag").ToString());
                        mapping.residence_country_rcd               = XmlHelper.XpathValueNullToEmpty(n3, "residence_country_rcd");
                        mapping.document_type_rcd                   = XmlHelper.XpathValueNullToEmpty(n3, "document_type_rcd");
                        mapping.passport_issue_country_rcd          = XmlHelper.XpathValueNullToEmpty(n3, "passport_issue_country_rcd");
                        
                    }
                }

                if (passengerId == "")
                {
                    mappings.Add(mapping);
                }
                else
                {
                    if (mapping.passenger_id.ToString() == passengerId)
                    {
                        mappings.Add(mapping);
                    }
                }

                //mappings.Add(mapping);
            }

            return mappings;
        }
        private APIRouteConfigs GetAPIRouteConfigs(APIRouteConfigs routes)
        {
            return routes;
        }
        private APIPassengerServices GetAPIPassengerServices(string bookingSegmentId, string passengerId, string strGetPassengerDetailXML, APIPassengerServices services)
        {
            Library objLi = new Library();
            //APIPassengerService service;

            XPathDocument xmlDoc = new XPathDocument(new StringReader(strGetPassengerDetailXML));
            XPathNavigator nv = xmlDoc.CreateNavigator();

            foreach (XPathNavigator n in nv.Select("Booking/Mapping[booking_segment_id = '" + bookingSegmentId + "']"))
            {
                APIPassengerService service = new APIPassengerService();
                service.passenger_id                            = XmlHelper.XpathValueNullToGUID(n, "passenger_id");
                service.booking_segment_id                      = XmlHelper.XpathValueNullToGUID(n, "booking_segment_id");

                foreach (XPathNavigator n2 in nv.Select("Booking/Service[booking_segment_id = '" + service.booking_segment_id.ToString() + "'][passenger_id = '"+service.passenger_id+"']"))
                {
                    service.passenger_segment_service_id        = XmlHelper.XpathValueNullToGUID(n2, "passenger_segment_service_id");
                    service.special_service_status_rcd          = XmlHelper.XpathValueNullToEmpty(n2, "special_service_status_rcd");
                    service.special_service_change_status_rcd   = XmlHelper.XpathValueNullToEmpty(n2, "special_service_change_status_rcd");
                    service.special_service_rcd                 = XmlHelper.XpathValueNullToEmpty(n2, "special_service_rcd");
                    service.service_text                        = XmlHelper.XpathValueNullToEmpty(n2, "service_text");
                    service.flight_id                           = XmlHelper.XpathValueNullToEmpty(n2, "flight_id");
                    service.fee_id                              = XmlHelper.XpathValueNullToEmpty(n2, "fee_id");
                    service.number_of_units                     = short.Parse(XmlHelper.XpathValueNullToZero(n2, "number_of_units").ToString());
                    service.origin_rcd                          = XmlHelper.XpathValueNullToEmpty(n2, "origin_rcd");
                    service.destination_rcd                     = XmlHelper.XpathValueNullToEmpty(n2, "destination_rcd");
                    service.display_name                        = XmlHelper.XpathValueNullToEmpty(n2, "display_name");
                }

                if (passengerId == "")
                {
                    services.Add(service);
                }
                else
                {
                    if (service.passenger_id.ToString() == passengerId)
                    {
                        services.Add(service);
                    }
                }
                //services.Add(service);
            }

            return services;
        }
        private APIPassengerFees GetAPIPassengerFees(string bookingSegmentId, string strGetPassengerDetailXML, APIPassengerFees fees)
        {
            Library objLi = new Library();
            //APIPassengerFee fee;

            XPathDocument xmlDoc = new XPathDocument(new StringReader(strGetPassengerDetailXML));
            XPathNavigator nv = xmlDoc.CreateNavigator();

            foreach (XPathNavigator n in nv.Select("Booking/Fee[booking_segment_id = '" + bookingSegmentId + "']"))
            {
                APIPassengerFee fee = new APIPassengerFee();
                fee.booking_fee_id                  = XmlHelper.XpathValueNullToGUID(n, "booking_fee_id");
                fee.fee_amount                      = XmlHelper.XpathValueNullToZero(n, "fee_amount");
                fee.booking_id                      = XmlHelper.XpathValueNullToGUID(n, "booking_id");
                fee.passenger_id                    = XmlHelper.XpathValueNullToGUID(n, "passenger_id");
                fee.currency_rcd                    = XmlHelper.XpathValueNullToEmpty(n, "currency_rcd");
                fee.acc_fee_amount                  = XmlHelper.XpathValueNullToZero(n, "acc_fee_amount");
                fee.fee_id                          = XmlHelper.XpathValueNullToGUID(n, "fee_id");
                fee.fee_rcd                         = XmlHelper.XpathValueNullToEmpty(n, "fee_rcd");
                fee.display_name                    = XmlHelper.XpathValueNullToEmpty(n, "display_name");
                fee.payment_amount                  = XmlHelper.XpathValueNullToZero(n, "payment_amount");
                fee.booking_segment_id              = XmlHelper.XpathValueNullToGUID(n, "booking_segment_id");
                fee.agency_code                     = XmlHelper.XpathValueNullToEmpty(n, "agency_code");
                fee.fee_category_rcd                = XmlHelper.XpathValueNullToEmpty(n, "fee_category_rcd");

                foreach (XPathNavigator n2 in nv.Select("Booking/Service[fee_id = '" + fee.fee_id.ToString() + "']"))
                {
                    fee.passenger_segment_service_id = XmlHelper.XpathValueNullToGUID(n2, "passenger_segment_service_id");
                }
                
                //fee.origin_rcd                      = XmlHelper.XpathValueNullToEmpty(n, "origin_rcd");
                //fee.destination_rcd                 = XmlHelper.XpathValueNullToEmpty(n, "destination_rcd");

                //foreach (XPathNavigator n2 in nv.Select("Booking/Mapping[booking_segment_id = '" + fee.booking_segment_id.ToString() + "'][passenger_id = '" + fee.passenger_id.ToString() + "']"))
                //{
                //    fee.od_origin_rcd               = XmlHelper.XpathValueNullToEmpty(n2, "od_origin_rcd");
                //    fee.od_destination_rcd          = XmlHelper.XpathValueNullToEmpty(n2, "od_destination_rcd");
                //}

                fees.Add(fee);
            }

            return fees;
        }
        private APIErrors GetAPIErrors(string code, APIErrors errors)
        {
            string message = string.Empty;

            string ServerName = string.Empty;

            if (System.Configuration.ConfigurationManager.AppSettings["ServerName"] != null)
            {
                ServerName = System.Configuration.ConfigurationManager.AppSettings["ServerName"];
            }


            switch (code)
            {
                case "000" :
                    message = "Successful transaction request";
                    break;
                case "100":
                    message = "Logon fail";
                    break;
                case "101":
                    message = "Not found booking logon information";
                    break;
                case "102":
                    message = "Have outstanding balance cannnot login";
                    break;
                case "103":
                    message = "Flight not open for checkin";
                    break;
                case "110":
                    message = "Invalid bookingref parameter";
                    break;
                case "111":
                    message = "Invalid lastname parameter";
                    break;
                case "200":
                    message = "GetPassengers error";
                    break;
                case "201":
                    message = "Not found passenger information";
                    break;
                case "210":
                    message = "Invalid bookingsegmentid parameter";
                    break;
                case "300":
                    message = "AssignSeats error";
                    break;
                case "301":
                    message = "Passengers could not be assigned seat";
                    break;
                case "302":
                    message = "Required logon before assign seat";
                    break;
                case "303":
                    message = "Child or infant can't assign seat independently";
                    break;
                case "304":
                    message = "Number of adults need to be higher than the number of infants";
                    break;
                case "305":
                    message = "OFFLOADED passenger can't checkin";
                    break;
                case "306":
                    message = "Not enough seat to assign";
                    break;
                case "307":
                    message = "This flight is free-seating";
                    break;
                case "310":
                    message = "Invalid BookingSegmentId parameter";
                    break;
                case "311":
                    message = "Invalid userid parameter";
                    break;
                case "312":
                    message = "Invalid PassengerId parameter";
                    break;
                case "313":
                    message = "Passengers could not be zero";
                    break;
                case "314":
                    message = "Passengers already have been assigned seat";
                    break;
                case "400":
                    message = "Commit error";
                    break;
                case "401":
                    message = "Passengers could not checked in.";
                    break;
                case "402":
                    message = "Required assign seat before check in";
                    break;
                case "403":
                    message = "Child or infant can't checkin independently";
                    break;
                case "404":
                    message = "Number of adults need to be higher than the number of infants";
                    break;
                case "405":
                    message = "OFFLOADED passenger can't checkin";
                    break;
                case "410":
                    message = "Invalid bookingsegmentid parameter";
                    break;
                case "411":
                    message = "Invalid passengerids parameter";
                    break;
                case "500":
                    message = "GetBoardingPassess error";
                    break;
                case "501":
                    message = "Required logon before get boarding pass";
                    break;
                case "502":
                    message = "Required check in before get boarding pass";
                    break;
                case "503":
                    message = "Not found passenger information";
                    break;
                case "510":
                    message = "Invalid bookingsegmentid parameter";
                    break;
                case "511":
                    message = "Invalid passengerids parameter";
                    break;
                case "600":
                    message = "Some of the passenger information can not be edit";
                    break;
                case "601":
                    message = "Passenger input not found";
                    break;
                case "602":
                    message = "Passenger ID is required";
                    break;
                case "603":
                    message = "Date of birth must be the past date";
                    break;
                case "604":
                    message = "Expiry date must be future date";
                    break;
                //getseatmap
                case "700":
                    message = "GetSeatMap error";
                    break;
                case "701":
                    message = "Required logon before select seat";
                    break;
                case "710":
                    message = "Parameter OriginRcd is empty";
                    break;
                case "711":
                    message = "Parameter DestionationRcd is empty";
                    break;
                case "712":
                    message = "Parameter FlightId is empty";
                    break;
                case "713":
                    message = "Parameter BoardingClass is empty";
                    break;
                case "714":
                    message = "Parameter BookingClass is empty";
                    break;
                case "715":
                    message = "No seat map information found";
                    break;
                //selectseat
                case "800":
                    message = "Select seat error";
                    break;
                case "801":
                    message = "Infant's seat does not match adult's seat";
                    break;
                case "802":
                    message = "Required logon before select seat";//"Required get passengers information before select seat";
                    break;
                case "803":
                    message = "Child or infant can't assign seat independently";
                    break;
                case "804":
                    message = "Number of adults need to be higher than the number of infants";
                    break;
                case "805":
                    message = "OFFLOADED passenger can't checkin";
                    break;
                case "806":
                    message = "Passengers already have been assigned seat";
                    break;
                case "807":
                    message = "This flight is free seating";
                    break;
                case "808":
                    message = "Passengers do not match booking_segment_id";
                    break;
                case "809":
                    message = "Duplicate seat selected";
                    break;
                case "810":
                    message = "SeatRequest is empty";
                    break;
                case "811":
                    message = "One of the request supply an invalid BookingSegmentId";
                    break;
                case "812":
                    message = "One of the request supply an invalid PassengerId";
                    break;
                //selectseat
                case "900":
                    message = "Invalid sequence or seat parameter";
                    break;
                case "901":
                    message = "Passenger check in status is not CHECKED";
                    break;
                case "902":
                    message = "Passenger check in status is not BOARDED";
                    break;
                case "903":
                    message = "Cannot board or un-board passenger";
                    break;
                case "904":
                    message = "Cannot board passenger";
                    break;
                case "905":
                    message = "Cannot un-board passenger";
                    break;
                case "906":
                    message = "Invalid BookingId parameter";
                    break;
                case "907":
                    message = "BoardPassenger error";
                    break;
                case "908":
                    message = "Booking is not found";
                    break;
                case "909":
                    message = "Flight not open or closed for board or un-board";
                    break;
                case "910":
                    message = "Invalid token";
                    break;
                case "911":
                    message = "Booking is in use on AVANTIK";
                    break;
                case "950":
                    message = "Invalid initial token";
                    break;
                case "951":
                    message = "Invalid AgencyCode parameter";
                    break;
                case "952":
                    message = "Invalid AgencyLogon parameter";
                    break;
                case "953":
                    message = "Invalid AgencyPassword parameter";
                    break;
                case "954":
                    message = "Agency is not allowed to use web check-in api";
                    break;
                case "955":
                    message = "Flight not open or closed for offload";
                    break;
                case "956":
                    message = "Can not offload passenger";
                    break;
                default :
                    message = "Internal error.";
                    break;

            }

            APIError error = new APIError();
            error.code = code;
            error.message = message;
            error.serverId = ServerName;
            errors.Add(error);

            return errors;
        }

        private APIErrors GetAPIErrors(string code, string newmessage, APIErrors errors)
        {
            string message = string.Empty;
            APIError error = new APIError();
            error.code = code;
            error.message = newmessage;
            errors.Add(error);
            return errors;
        }

        private APISeatMaps GetAPISeatMaps(string flight_id, string strSeatMapXML, APISeatMaps seatmaps)
        {
            Library objLi = new Library();
            //APISeatMap seatmap;

            XPathDocument xmlDoc = new XPathDocument(new StringReader(strSeatMapXML));
            XPathNavigator nv = xmlDoc.CreateNavigator();

            XPathNodeIterator nodes = nv.Select("SeatMaps/SeatMap[flight_id = '" + flight_id + "']");
            if (nodes.Count == 0)
            {
                //seatmap layout for transit flight
                nodes = nv.Select("SeatMapLayout/Attribute[flight_id = '" + flight_id + "']");
            }
            //foreach (XPathNavigator n in nv.Select("SeatMaps/SeatMap[flight_id = '" + flight_id + "']"))
            foreach (XPathNavigator n in nodes)
            {
                APISeatMap seatmap = new APISeatMap();
                seatmap.flight_id = XmlHelper.XpathValueNullToGUID(n, "flight_id");
                seatmap.free_seating_flag = int.Parse(XmlHelper.XpathValueNullToZero(n, "free_seating_flag").ToString());
                seatmap.flight_check_in_status_rcd = XmlHelper.XpathValueNullToEmpty(n, "flight_check_in_status_rcd");
                seatmap.origin_rcd = XmlHelper.XpathValueNullToEmpty(n, "origin_rcd");
                seatmap.destination_rcd = XmlHelper.XpathValueNullToEmpty(n, "destination_rcd");
                seatmap.aircraft_configuration_code = XmlHelper.XpathValueNullToEmpty(n, "aircraft_configuration_code");
                seatmap.number_of_bays = int.Parse(XmlHelper.XpathValueNullToZero(n, "number_of_bays").ToString());
                seatmap.boarding_class_rcd = XmlHelper.XpathValueNullToEmpty(n, "boarding_class_rcd");
                seatmap.number_of_rows = int.Parse(XmlHelper.XpathValueNullToZero(n, "number_of_rows").ToString());
                seatmap.number_of_columns = int.Parse(XmlHelper.XpathValueNullToZero(n, "number_of_columns").ToString());
                seatmap.layout_row = int.Parse(XmlHelper.XpathValueNullToZero(n, "layout_row").ToString());
                seatmap.layout_column = int.Parse(XmlHelper.XpathValueNullToZero(n, "layout_column").ToString());
                seatmap.location_type_rcd = XmlHelper.XpathValueNullToEmpty(n, "location_type_rcd");
                seatmap.seat_column = XmlHelper.XpathValueNullToEmpty(n, "seat_column");
                seatmap.seat_row = XmlHelper.XpathValueNullToEmpty(n, "seat_row");
                seatmap.stretcher_flag = int.Parse(XmlHelper.XpathValueNullToZero(n, "stretcher_flag").ToString());
                seatmap.handicapped_flag = int.Parse(XmlHelper.XpathValueNullToZero(n, "handicapped_flag").ToString());
                seatmap.no_child_flag = int.Parse(XmlHelper.XpathValueNullToZero(n, "no_child_flag").ToString());
                seatmap.bassinet_flag = int.Parse(XmlHelper.XpathValueNullToZero(n, "bassinet_flag").ToString());
                seatmap.no_infant_flag = int.Parse(XmlHelper.XpathValueNullToZero(n, "no_infant_flag").ToString());
                seatmap.infant_flag = int.Parse(XmlHelper.XpathValueNullToZero(n, "infant_flag").ToString());
                seatmap.emergency_exit_flag = int.Parse(XmlHelper.XpathValueNullToZero(n, "emergency_exit_flag").ToString());
                seatmap.unaccompanied_minors_flag = int.Parse(XmlHelper.XpathValueNullToZero(n, "unaccompanied_minors_flag").ToString());
                seatmap.window_flag = int.Parse(XmlHelper.XpathValueNullToZero(n, "window_flag").ToString());
                seatmap.aisle_flag = int.Parse(XmlHelper.XpathValueNullToZero(n, "aisle_flag").ToString());
                seatmap.block_b2c_flag = int.Parse(XmlHelper.XpathValueNullToZero(n, "block_b2c_flag").ToString());
                seatmap.block_b2b_flag = int.Parse(XmlHelper.XpathValueNullToZero(n, "block_b2b_flag").ToString());
                seatmap.blocked_flag = int.Parse(XmlHelper.XpathValueNullToZero(n, "blocked_flag").ToString());
                seatmap.low_comfort_flag = int.Parse(XmlHelper.XpathValueNullToZero(n, "low_comfort_flag").ToString());
                seatmap.passenger_count = int.Parse(XmlHelper.XpathValueNullToZero(n, "passenger_count").ToString());

                seatmaps.Add(seatmap);
            }

            return seatmaps;
        }
        public DataSet CacheDestination()
        {
            DataSet ds = new DataSet();
            try
            {
                Library objLi = new Library();
                ds = (DataSet)HttpRuntime.Cache["destination-" + tikAEROWebCheckinAPI.Classes.Language.CurrentCode().ToUpper()];
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    return ds;
                }
                else
                {
                    ServiceClient objClient = new ServiceClient();

                    Booking objBooking = new Booking();
                    //ds = objClient.GetSessionlessDestination(tikAEROWebCheckinAPI.Classes.Language.CurrentCode().ToUpper(), true, false, false, false, false, objBooking.GenerateSessionlessToken());
                    ds = objClient.GetSessionlessDestination(tikAEROWebCheckinAPI.Classes.Language.CurrentCode().ToUpper(), true, false, false, false, false, SecurityHelper.GenerateSessionlessToken());

                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        HttpRuntime.Cache.Insert("destination-" + tikAEROWebCheckinAPI.Classes.Language.CurrentCode().ToUpper(), ds, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(20), System.Web.Caching.CacheItemPriority.Normal, null);
                    }
                    return ds;
                }
            }
            catch (Exception ex)
            {
                //Helper objHp = new Helper();
                //objHp.SendErrorEmail(ex, string.Empty);
                throw ex;
            }
        }
        private bool IsSuccess(APIResult result)
        {
            bool bResult = false;
            if (result != null && result.APIErrors[0].code.Equals("000"))
            {
                bResult = true;
            }
            return bResult;
        }
        private bool IsClearSeat(Mappings mappings)
        {
            bool bResult = false;
            if (mappings != null)
            {
                foreach(Mapping mapping in mappings)
                {
                    if (!string.IsNullOrEmpty(mapping.seat_number))
                    {
                        mapping.seat_number = string.Empty;
                        mapping.seat_row    = 0;
                        mapping.seat_column = string.Empty;
                    }
                }

                Session["mappings"] = mappings;
                bResult = true;
            }
            return bResult;
        }
        #endregion

        //[WebMethod(EnableSession = true, Description = "Test GetSeatmapLayout")]
        //public APIResult TestGetSeatmapLayout()
        //{
        //    return GetSeatMapLayout("DFBEFE03-B0EE-4E84-BC9F-1D9690CE10C6", "ACH", "FDH", "Y", "", "EN");
        //}

        //[WebMethod(EnableSession = true, Description = "Test AssignSeatLayout")]
        //public APIResult TestAssignSeatLayout()
        //{
        //    string[] strPassengerIds = {
        //                                  "e52326f4-e66d-40b2-89b6-bf8bb0b3afb1",
        //                                  "32f69df2-cb3a-48b4-b3a6-8dbc7c9b8496"
        //                              };
        //   return AssignSeats("db2a2234-e8e6-494a-bc81-60bbc5f6ae36", strPassengerIds);
        //}
    }
}
